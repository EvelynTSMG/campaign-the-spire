package EvesActMod.cards;

import EvesActMod.common.Option;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.helpers.CardLibrary;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static EvesActMod.EvesActMod.id;

public class CampaignDeck {
    public static final String STARTER_DECK_NAME = CardCrawlGame.languagePack.getUIString(id("StarterDeckName")).TEXT[0];

    public static final int MIN_CARDS = 10;
    public static final int MAX_CARDS = 30;
    public static final int MAX_COPIES = 4;
    public static final Map<AbstractCard.CardRarity, Integer> MIN_CARDS_WITH = new HashMap<>();
    public static final Map<AbstractCard.CardRarity, Integer> MAX_COPIES_OF = new HashMap<>();

    public String name;
    public ArrayList<AbstractCard> cards;
    public AbstractPlayer.PlayerClass color;

    private CampaignDeck() { }

    public static CampaignDeck from_starter_deck(AbstractPlayer player) {
        CampaignDeck deck = new CampaignDeck();

        deck.name = STARTER_DECK_NAME;
        deck.cards = player.getStartingDeck()
                .stream()
                .map(CardLibrary::getCard)
                .collect(Collectors.toCollection(ArrayList::new));
        deck.color = player.chosenClass;

        return deck;
    }

    public boolean validate() {
        int min_cards = Option.from(cards.stream()
                .map(card -> MIN_CARDS_WITH.get(card.rarity)).max(Integer::compare))
                .unwrap_or(MIN_CARDS);

        if (cards.size() < min_cards) return false;

        final boolean[] too_many_copies = {false};
        cards.forEach(card -> {
            if (cards.stream().filter(c -> c.cardID.equals(card.cardID)).count() > MAX_COPIES_OF.getOrDefault(card.rarity, MAX_COPIES)) {
                too_many_copies[0] = true;
            }
        });
        return !too_many_copies[0];
    }

    public boolean can_add(AbstractCard card) {
        if (cards.size() >= MAX_CARDS) return false;
        int card_copies = (int)cards.stream().filter(c -> c.cardID.equals(card.cardID)).count();
        return card_copies < MAX_COPIES_OF.getOrDefault(card.rarity, MAX_COPIES);
    }

    static {
        MIN_CARDS_WITH.put(AbstractCard.CardRarity.UNCOMMON, 15);
        MIN_CARDS_WITH.put(AbstractCard.CardRarity.RARE, 20);
        MAX_COPIES_OF.put(AbstractCard.CardRarity.BASIC, 5);
        MAX_COPIES_OF.put(AbstractCard.CardRarity.COMMON, 3);
        MAX_COPIES_OF.put(AbstractCard.CardRarity.UNCOMMON, 2);
        MAX_COPIES_OF.put(AbstractCard.CardRarity.RARE, 1);
    }
}
