package EvesActMod.books;

import EvesActMod.EvesActMod;
import EvesActMod.common.errors.UnexpectedStateError;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

import java.lang.reflect.Field;

import static EvesActMod.EvesActMod.shader_path;

public class ShaderBook {
    // Utility functions
    public static String shader_at(String path) {
        return Gdx.files.internal(shader_path(path)).readString();
    }

    public static ShaderProgram shader_named(String name) {
        return new ShaderProgram(shader_at(name + ".vert"), shader_at(name + ".frag"));
    }

    public static ShaderProgram frag_shader_named(String name) {
        return new ShaderProgram(shader_at("default.vert"), shader_at(name + ".frag"));
    }

    // Shaders!
    public static final ShaderProgram DEFAULT = shader_named("default");
    public static final ShaderProgram GRAYSCALE = frag_shader_named("grayscale");
    public static final ShaderProgram INVERT = frag_shader_named("invert");
    public static final ShaderProgram VIGNETTE_WHITE = frag_shader_named("vignette_white");
    public static final ShaderProgram FLYERS_FIRE = frag_shader_named("flyers_fire");
    public static final ShaderProgram FOG = frag_shader_named("fog");
    public static final ShaderProgram LIGHT_BEAMS = frag_shader_named("light_beams");

    static {
        // Iterate over all declared ShaderPrograms and be mad if they failed to compile, or publish their logs
        Field[] fields = ShaderBook.class.getDeclaredFields();
        for (Field field : fields) {
            if (field.getType() == ShaderProgram.class) {
                ShaderProgram shader;

                try {
                    shader = (ShaderProgram)field.get(null);
                } catch (IllegalAccessException e) { throw new RuntimeException(e); }

                if (!shader.isCompiled())  {
                    throw new UnexpectedStateError(
                        String.format("Shader %s failed to compile:\n%s", field.getName(), shader.getLog())
                    );
                }

                if (!shader.getLog().isEmpty()) {
                    // Shader-printed warnings or something
                    EvesActMod.logger.warn(String.format("Shader %s logs:\n%s", field.getName(), shader.getLog()));
                }
            }
        }
    }
}
