package EvesActMod.books;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;
import java.util.Map;

import static EvesActMod.EvesActMod.sound_path;

public class AudioBook {
    public enum SoundCategory {
        BGM,
        SFX
    }

    public static Map<Sound, SoundCategory> sound_map = new HashMap<>();

    // Utility functions
    private static Sound sound_at(String path) {
        return Gdx.audio.newSound(Gdx.files.internal(sound_path(path)));
    }

    public static Sound bgm_at(String path) {
        Sound sound = sound_at("bgm/" + path);
        sound_map.put(sound, SoundCategory.BGM);
        return sound;
    }

    public static Sound sfx_at(String path) {
        Sound sound = sound_at("sfx/" + path);
        sound_map.put(sound, SoundCategory.SFX);
        return sound;
    }

    public static Sound UI_HOVER = sfx_at("ui_hover.wav");
}
