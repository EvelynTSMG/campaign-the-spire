package EvesActMod.common;

import EvesActMod.common.errors.UnexpectedStateError;
import jdk.internal.vm.annotation.ForceInline;

import java.util.NoSuchElementException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static EvesActMod.common.Option.*;

@SuppressWarnings("unused")
public class Result<T, E> {
    // Invariant: Exactly one of these is `null` at all times.
    private T value;
    private E error;

    private Result(T value, E error) {
        this.value = value;
        this.error = error;
    }

    private void set(T value, E error) {
        this.value = value;
        this.error = error;
    }

    @ForceInline
    public static <T, E> Result<T, E> Ok(T value) {
        return new Result<>(value, null);
    }

    @ForceInline
    public static <T, E> Result<T, E> Err(E error) {
        return new Result<>(null, error);
    }

    @ForceInline
    public boolean is_ok() {
        return value != null;
    }

    @ForceInline
    public boolean is_ok_and(Predicate<T> f) {
        return value != null && f.test(value);
    }

    @ForceInline
    public boolean is_err() {
        return error != null;
    }

    @ForceInline
    public boolean is_err_and(Predicate<E> f) {
        return error != null && f.test(error);
    }

    @ForceInline
    public Option<T> ok() {
        return value != null ? Some(value) : None();
    }

    @ForceInline
    public Option<E> err() {
        return error != null ? Some(error) : None();
    }

    @ForceInline
    public <U> Result<U, E> map(Function<T, U> op) {
        return value != null ? Ok(op.apply(value)) : Err(error);
    }

    @ForceInline
    public <U> U map_or(Function<T, U> op, U def) {
        return value != null ? op.apply(value) : def;
    }

    @ForceInline
    public <U> U map_or_else(Function<T, U> op, Function<E, U> def) {
        return value != null ? op.apply(value) : def.apply(error);
    }

    @ForceInline
    public <F> Result<T, F> map_err(Function<E, F> op) {
        return value != null ? Ok(value) : Err(op.apply(error));
    }

    @ForceInline
    public Result<T, E> inspect(Consumer<? super T> f) {
        if (value != null) f.accept(value);
        return this;
    }

    @ForceInline
    public Result<T, E> inspect_err(Function<E, E> f) {
        if (error != null) error = f.apply(error);
        return this;
    }

    @ForceInline
    public T expect(String msg) {
        if (value != null) return value;
        throw new UnexpectedStateError(msg);
    }

    @ForceInline
    public T unwrap() {
        if (value != null) return value;
        throw new NoSuchElementException("called `Result::unwrap()` on an `Err` value");
    }

    @ForceInline
    public E expect_err(String msg) {
        if (error != null) return error;
        throw new UnexpectedStateError(msg);
    }

    @ForceInline
    public E unwrap_err() {
        if (error != null) return error;
        throw new NoSuchElementException("called `Result::unwrap_err()` on an `Ok` value");
    }

    @ForceInline
    public <U> Result<U, E> and(Result<U, E> res) {
        return value != null ? res : Err(error);
    }

    @ForceInline
    public <U> Result<U, E> and_then(Function<T, Result<U, E>> op) {
        return value != null ? op.apply(value) : Err(error);
    }

    @ForceInline
    public Result<T, E> or(Result<T, E> res) {
        return value != null ? this : res;
    }

    @ForceInline
    public <F> Result<T, F> or_else(Function<E, Result<T, F>> op)  {
        return value != null ? Ok(value) : op.apply(error);
    }

    @ForceInline
    public T unwrap_or(T def) {
        return value != null ? value : def;
    }

    @ForceInline
    public T unwrap_or_else(Function<E, T> op) {
        return value != null ? value : op.apply(error);
    }

    @ForceInline
    public T unwrap_unchecked() {
        return value;
    }

    @ForceInline
    public E unwrap_err_unchecked() {
        return error;
    }
}
