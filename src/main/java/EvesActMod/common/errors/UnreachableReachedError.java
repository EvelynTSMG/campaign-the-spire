package EvesActMod.common.errors;

public class UnreachableReachedError extends Error {
    public UnreachableReachedError(String msg) {
        super(msg);
    }
}

