package EvesActMod.common.errors;

public class UnexpectedStateError extends Error {
    public UnexpectedStateError(String msg) {
        super(msg);
    }

    public UnexpectedStateError(String format, Object... args) {
        super(String.format(format, args));
    }
}
