package EvesActMod;

import EvesActMod.areas.CampaignArea;
import EvesActMod.books.ShaderBook;
import basemod.BaseMod;
import basemod.interfaces.EditStringsSubscriber;
import basemod.interfaces.PostInitializeSubscriber;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.evacipated.cardcrawl.modthespire.lib.SpireInitializer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.localization.UIStrings;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.Map;

@SuppressWarnings({"unused", "WeakerAccess"})
@SpireInitializer
public class EvesActMod implements
        EditStringsSubscriber,
        PostInitializeSubscriber {
    public static final String MOD_ID = EvesActMod.class.getSimpleName();
    public static final Logger logger = LogManager.getLogger(MOD_ID);

    public static String PLACEHOLDER_TEXT;

    public static String id(String text) {
        return MOD_ID + ":" + text;
    }

    public static String path(String path) {
        return MOD_ID + "Resources/" + path;
    }

    public static String image_path(String path) {
        return MOD_ID + "Resources/images/" + path;
    }

    public static String shader_path(String path) {
        return MOD_ID + "Resources/shaders/" + path;
    }

    public static String sound_path(String path) {
        return MOD_ID + "Resources/audio/" + path;
    }

    public static String lang_path(String path) {
        return MOD_ID + "Resources/localization/" + get_lang_string() + "/" + path;
    }

    // If you support any more languages, add them here
    public static Settings.GameLanguage[] supported_languages = {
            Settings.GameLanguage.ENG,
    };

    private static String get_lang_string() {
        for (Settings.GameLanguage lang : supported_languages) {
            if (lang.equals(Settings.language)) {
                return Settings.language.name().toLowerCase();
            }
        }
        return "eng";
    }

    public EvesActMod() {
        BaseMod.subscribe(this);
    }

    public static void initialize() {
        new EvesActMod();
    }

    @Override
    public void receivePostInitialize() {
        PLACEHOLDER_TEXT = CardCrawlGame.languagePack.getUIString(id("Util_Placeholder")).TEXT[0];

        // Initialize stuff on game load instead of on clicking
        // the "Campaign" button in the main menu
        ShaderProgram a = ShaderBook.DEFAULT;
        CardCampaignGame.init();
    }

    @Override
    public void receiveEditStrings() {
        BaseMod.loadCustomStringsFile(UIStrings.class, lang_path("ui.json"));

        Gson gson =  new Gson();
        String json = Gdx.files.internal(lang_path("areas.json"))
                .readString(String.valueOf(StandardCharsets.UTF_8));
        Type type = new TypeToken<Map<String, CampaignArea.AreaStrings>>() {}.getType();
        Map<String, CampaignArea.AreaStrings> areas = gson.fromJson(json, type);
        areas.forEach(CampaignArea.AreaStrings::add);
    }
}
