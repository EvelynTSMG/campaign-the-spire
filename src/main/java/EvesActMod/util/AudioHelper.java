package EvesActMod.util;

import EvesActMod.books.AudioBook;
import com.badlogic.gdx.audio.Sound;
import com.megacrit.cardcrawl.core.Settings;

public class AudioHelper {
    public static float get_volume(AudioBook.SoundCategory category) {
        switch (category) {
            case BGM: return Settings.MUSIC_VOLUME;
            case SFX: return Settings.SOUND_VOLUME;
        }

        return Settings.MASTER_VOLUME;
    }

    public static long play(Sound sound, float volume) {
        return sound.play(volume * get_volume(AudioBook.sound_map.get(sound)));
    }

    public static long loop(Sound sound, float volume) {
        return sound.loop(volume * get_volume(AudioBook.sound_map.get(sound)));
    }
}
