package EvesActMod.util;

import EvesActMod.CardCampaignGame;
import EvesActMod.actions.TimedVFXAction;
import basemod.BaseMod;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.brashmonkey.spriter.Player;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.animations.VFXAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.DamageAction;
import com.megacrit.cardcrawl.actions.common.DiscardAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInDrawPileAction;
import com.megacrit.cardcrawl.actions.common.MakeTempCardInHandAction;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.cards.CardGroup;
import com.megacrit.cardcrawl.cards.DamageInfo;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.input.InputHelper;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.random.Random;
import com.megacrit.cardcrawl.rooms.AbstractRoom;
import com.megacrit.cardcrawl.vfx.AbstractGameEffect;

import java.util.ArrayList;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class Wiz {
    public static AbstractPlayer player() {
        return AbstractDungeon.player;
    }

    /**
     * Run a lambda on all cards in cardsList.
     * @deprecated
     * Write a for loop manually.
     */
    @Deprecated
    public static void forAllCardsInList(Consumer<AbstractCard> consumer, ArrayList<AbstractCard> cardsList) {
        for (AbstractCard c : cardsList) {
            consumer.accept(c);
        }
    }

    /**
     * Ignores CARD_POOL and UNSPECIFIED card group types.
     *
     * @param groups The card groups to get cards from
     * @return The cards from the specified groups
     */
    public static ArrayList<AbstractCard> cards_in_groups(CardGroup.CardGroupType... groups) {
        ArrayList<AbstractCard> cards = new ArrayList<>();

        for (CardGroup.CardGroupType group : groups) {
            switch (group) {
                case MASTER_DECK:
                    cards.addAll(player().masterDeck.group);
                    break;
                case DRAW_PILE:
                    cards.addAll(player().drawPile.group);
                    break;
                case DISCARD_PILE:
                    cards.addAll(player().discardPile.group);
                    break;
                case EXHAUST_PILE:
                    cards.addAll(player().exhaustPile.group);
                    break;
                case HAND:
                    cards.addAll(player().hand.group);
                    break;
            }
        }

        return cards;
    }

    /**
     * Run a lambda on all alive monsters.
     * @deprecated
     * Write a for loop manually.
     */
    @Deprecated
    public static void forAllMonstersLiving(Consumer<AbstractMonster> consumer) {
        for (AbstractMonster m : alive_monsters()) {
            consumer.accept(m);
        }
    }

    public static ArrayList<AbstractMonster> alive_monsters() {
        ArrayList<AbstractMonster> monsters = new ArrayList<>(AbstractDungeon.getMonsters().monsters);
        monsters.removeIf(m -> m.isDead || m.isDying);
        return monsters;
    }

    public static ArrayList<AbstractCard> cards(Predicate<AbstractCard> filter) {
        return cards(filter, false);
    }

    public static ArrayList<AbstractCard> cards(Predicate<AbstractCard> filter, boolean from_all_cards) {
        ArrayList<AbstractCard> cards = new ArrayList<>();

        if (from_all_cards) {
            for (AbstractCard c : CardLibrary.getAllCards()) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
        } else {
            for (AbstractCard c : AbstractDungeon.srcCommonCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
            for (AbstractCard c : AbstractDungeon.srcUncommonCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
            for (AbstractCard c : AbstractDungeon.srcRareCardPool.group) {
                if (filter.test(c)) cards.add(c.makeStatEquivalentCopy());
            }
        }

        return cards;
    }

    public static Optional<AbstractCard> random_card(Predicate<AbstractCard> filter, boolean from_all_cards) {
        return random_item(cards(filter, from_all_cards));
    }

    public static Optional<AbstractCard> random_card(Predicate<AbstractCard> filter) {
        return random_card(filter, false);
    }

    /**
     * Ignores CARD_POOL and UNSPECIFIED card group types.
     *
     * @param filter Filter to apply to the cards
     * @param groups The card groups to get cards from
     * @return A random card from the specified groups, or Empty if all the groups were empty
     */
    public static Optional<AbstractCard> random_card_from(Predicate<AbstractCard> filter, CardGroup.CardGroupType... groups) {
        return random_item(cards_in_groups(groups)
                .stream()
                .filter(filter)
                .collect(Collectors.toCollection(ArrayList::new)));
    }

    public static <T> Optional<T> random_item(ArrayList<T> from, Random rng) {
        return from.isEmpty() ? Optional.empty() : Optional.ofNullable(from.get(rng.random(from.size() - 1)));
    }

    public static <T> Optional<T> random_item(ArrayList<T> from) {
        return random_item(from, AbstractDungeon.cardRandomRng);
    }

    private static boolean is_hovered(Hitbox hb) {
        return InputHelper.mX > hb.x
            && InputHelper.mX < hb.x + hb.width
            && InputHelper.mY > hb.y
            && InputHelper.mY < hb.y + hb.height;
    }

    public static boolean is_in_combat() {
        return CardCrawlGame.isInARun()
            && AbstractDungeon.currMapNode != null
            && AbstractDungeon.getCurrRoom() != null
            && AbstractDungeon.getCurrRoom().phase == AbstractRoom.RoomPhase.COMBAT;
    }

    public static void atb(AbstractGameAction action) {
        AbstractDungeon.actionManager.addToBottom(action);
    }

    public static void att(AbstractGameAction action) {
        AbstractDungeon.actionManager.addToTop(action);
    }

    public static void vfx(AbstractGameEffect effect) {
        atb(new VFXAction(effect));
    }

    public static void vfx(AbstractGameEffect effect, float duration) {
        atb(new VFXAction(effect, duration));
    }

    public static void tfx(AbstractGameEffect effect) {
        atb(new TimedVFXAction(effect));
    }

    public static void make_card_hand(AbstractCard c, int amount) {
        atb(new MakeTempCardInHandAction(c, amount));
    }

    public static void make_card_hand(AbstractCard c) {
        make_card_hand(c, 1);
    }

    public static void make_card_shuffle(AbstractCard c, int amount) {
        atb(new MakeTempCardInDrawPileAction(c, amount, true, true));
    }

    public static void make_card_shuffle(AbstractCard c) {
        make_card_shuffle(c, 1);
    }

    public static void make_card_top(AbstractCard c, int amount) {
        atb(new MakeTempCardInDrawPileAction(c, amount, false, true));
    }

    public static void make_card_top(AbstractCard c) {
        make_card_top(c, 1);
    }

    public static void apply_enemy(AbstractMonster target, AbstractPower power) {
        atb(new ApplyPowerAction(target, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_enemy_now(AbstractMonster target, AbstractPower power) {
        att(new ApplyPowerAction(target, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_self(AbstractPower power) {
        atb(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, power, power.amount));
    }

    public static void apply_self_now(AbstractPower power) {
        att(new ApplyPowerAction(AbstractDungeon.player, AbstractDungeon.player, power, power.amount));
    }

    public static void thorn_dmg(AbstractCreature target, int amount, AbstractGameAction.AttackEffect atk_fx) {
        atb(new DamageAction(target, new DamageInfo(AbstractDungeon.player, amount, DamageInfo.DamageType.THORNS), atk_fx));
    }

    public static void thorn_dmg(AbstractCreature target, int amount) {
        thorn_dmg(target, amount, AbstractGameAction.AttackEffect.NONE);
    }

    public static void discard(int amount, boolean isRandom) {
        atb(new DiscardAction(player(), player(), amount, isRandom));
    }

    public static void discard(int amount) {
        discard(amount, false);
    }

    public static Optional<Integer> power_amount(AbstractCreature of, Class<? extends AbstractPower> power) {
        for (AbstractPower p : of.powers) {
            if (p.getClass().equals(power)) {
                return Optional.of(p.amount);
            }
        }

        return Optional.empty();
    }

    public static <T> Predicate<T> distinct_by(Function<? super T, ?> key_extractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(key_extractor.apply(t), Boolean.TRUE) == null;
    }

    public static String get_cost_text(int cost) {
        switch (cost) {
            case -2: return "U";
            case -1: return "X";
            default: return String.valueOf(cost);
        }
    }

    public static int compare_cards(AbstractCard a, AbstractCard b) {
        if (a.color.ordinal() > b.color.ordinal()) return 1;
        if (a.color.ordinal() < b.color.ordinal()) return -1;

        if (a.rarity.ordinal() > b.rarity.ordinal()) return 1;
        if (a.rarity.ordinal() < b.rarity.ordinal()) return -1;

        if (a.type.ordinal() > b.type.ordinal()) return 1;
        if (a.type.ordinal() < b.type.ordinal()) return -1;

        if (a.cost > b.cost) return 1;
        if (a.cost < b.cost) return -1;

        return a.name.compareToIgnoreCase(b.name);
    }

    public static Hitbox centered_hitbox(float cX, float cY, float width, float height) {
        return new Hitbox(cX - width/2f, cY - height/2f, width, height);
    }

    public static float lerp(float start, float target, float speed, float snap_threshold) {
        if (start != target) {
            start = MathUtils.lerp(start, target, Gdx.graphics.getDeltaTime() * speed);
            if (Math.abs(start - target) < snap_threshold) {
                return target;
            }
        }

        return start;
    }

    public static String get_character_name(AbstractPlayer.PlayerClass cls) {
        return CardCampaignGame.get_character(cls).getLocalizedCharacterName();
    }

    public static Color get_character_color(AbstractPlayer.PlayerClass cls) {
        switch (cls) {
            case IRONCLAD: return Color.valueOf("#ff6563");
            case THE_SILENT: return Color.valueOf("#7fff00");
            case DEFECT: return Color.valueOf("#87ceeb");
            case WATCHER: return Color.valueOf("#ee82ee");
            default: return BaseMod.getBgColor(CardCampaignGame.get_character(cls).getCardColor());
        }
    }

    public static float[] vec3_from(Color value) {
        return new float[] { value.r, value.g, value.b };
    }

    @SafeVarargs
    public static <T extends Comparable<T>> T min(T... values) {
        T min = values[0];
        for (int i = values.length; i > 0;) {
            if (values[--i].compareTo(min) < 0) min = values[i];
        }
        return min;
    }

    @SafeVarargs
    public static <T extends Comparable<T>> T max(T... values) {
        T max = values[0];
        for (int i = values.length; i > 0;) {
            if (values[--i].compareTo(max) > 0) max = values[i];
        }
        return max;
    }

    public static Color invert(Color value) {
        return new Color(1f - value.r, 1f - value.g, 1f - value.b, value.a);
    }
}
