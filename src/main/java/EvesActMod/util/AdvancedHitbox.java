package EvesActMod.util;

import EvesActMod.ui.Renderable;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

public class AdvancedHitbox extends Hitbox implements Renderable {
    private static final ShapeRenderer sr = new ShapeRenderer();

    public float scale = 1f;
    public float offset_x;
    public float offset_y;

    public AdvancedHitbox(float width, float height) {
        super(width, height);
    }

    public AdvancedHitbox(float x, float y, float width, float height) {
        super(x, y, width, height);
    }

    public AdvancedHitbox(float x, float y, float width, float height, float scale) {
        super(x, y, width, height);
        this.scale = scale;
    }

    public static AdvancedHitbox from(Hitbox hb) {
        return new AdvancedHitbox(hb.x, hb.y, hb.width, hb.height, 1f);
    }

    public static AdvancedHitbox centered(float x, float y, float width, float height) {
        return new AdvancedHitbox(x - width/2f, y - height/2f, width, height);
    }

    public static AdvancedHitbox centered(float x, float y, float width, float height, float scale) {
        return new AdvancedHitbox(x - width/2f, y - height/2f, width, height, scale);
    }

    protected float get_scaled_adjx() {
        return ((scale * width) - width)/2f;
    }

    protected float get_scaled_adjy() {
        return ((scale * height) - height)/2f;
    }

    public float width() {
        return width + get_scaled_adjx() * 2;
    }

    public float height()  {
        return height + get_scaled_adjy() * 2;
    }

    public float left() {
        return x + offset_x - get_scaled_adjx();
    }

    public float bottom() {
        return y + offset_y - get_scaled_adjy();
    }

    public float right() {
        return left() + width();
    }

    public float top() {
        return bottom() + height();
    }

    public float cX() {
        return cX + offset_x;
    }

    public float cY() {
        return cY + offset_y;
    }

    public void clamp_pos(float left, float right, float bottom, float top) {
        move(MathUtils.clamp(cX, left, right), MathUtils.clamp(cY, bottom, top));
    }

    @Override
    public void update(float x, float y) {
        if (!AbstractDungeon.isFadingOut) {
            this.x = x;
            this.y = y;

            boolean was_hovered = hovered;

            hovered = InputHelper.mX > left()
                && InputHelper.mX < right()
                && InputHelper.mY > bottom()
                && InputHelper.mY < top();

            justHovered = !was_hovered && hovered;
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        if (Settings.isDebug || Settings.isInfo) {
            sb.end();
            sr.begin(ShapeRenderer.ShapeType.Line);
            sr.setColor(hovered ? InputHelper.isMouseDown ? Color.CHARTREUSE : InputHelper.isMouseDown_R ? Color.CYAN : Color.GOLDENROD : Color.RED);

            sr.rect(left(), bottom(), width(), height());
            sr.line(left(), top(), right(), bottom());
            sr.line(left(), bottom(), right(), top());

            sr.end();
            sb.begin();
        }
    }

    @Override
    public boolean intersects(Hitbox other) {
        return left() < other.x + other.width
                && right() > other.x
                && bottom() < other.y + other.height
                && top() > other.y;
    }

    public boolean intersects(AdvancedHitbox other) {
        return left() < other.right()
            && right() > other.left()
            && bottom() < other.top()
            && top() > other.bottom();
    }

    public boolean is_on_screen() {
        return  left() < Settings.WIDTH
                && right() > 0
                && bottom() < Settings.HEIGHT
                && top() > 0;
    }
}
