package EvesActMod.util;

import basemod.ReflectionHacks;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.localization.LocalizedStrings;

import java.util.HashMap;

import static EvesActMod.EvesActMod.id;

@SuppressWarnings("unused")
public class TextHelper {
    public static String TEXT_CUTOFF = CardCrawlGame.languagePack.getUIString(id("Util_TextCutoff")).TEXT[0];
    
    // Fonts copied from FontHelper
    public static final String TINY_NUMBERS_FONT = "font/04b03.ttf";
    public static final String ENG_DEFAULT_FONT = "font/Kreon-Regular.ttf";
    public static final String ENG_BOLD_FONT = "font/Kreon-Bold.ttf";
    public static final String ENG_ITALIC_FONT = "font/ZillaSlab-RegularItalic.otf";
    public static final String ENG_DRAMATIC_FONT = "font/FeDPrm27C.otf";
    public static final String ZHS_DEFAULT_FONT = "font/zhs/NotoSansMonoCJKsc-Regular.otf";
    public static final String ZHS_BOLD_FONT = "font/zhs/SourceHanSerifSC-Bold.otf";
    public static final String ZHS_ITALIC_FONT = "font/zhs/SourceHanSerifSC-Medium.otf";
    public static final String ZHT_DEFAULT_FONT = "font/zht/NotoSansCJKtc-Regular.otf";
    public static final String ZHT_BOLD_FONT = "font/zht/NotoSansCJKtc-Bold.otf";
    public static final String ZHT_ITALIC_FONT = "font/zht/NotoSansCJKtc-Medium.otf";
    public static final String EPO_DEFAULT_FONT = "font/epo/Andada-Regular.otf";
    public static final String EPO_BOLD_FONT = "font/epo/Andada-Bold.otf";
    public static final String EPO_ITALIC_FONT = "font/epo/Andada-Italic.otf";
    public static final String GRE_DEFAULT_FONT = "font/gre/Roboto-Regular.ttf";
    public static final String GRE_BOLD_FONT = "font/gre/Roboto-Bold.ttf";
    public static final String GRE_ITALIC_FONT = "font/gre/Roboto-Italic.ttf";
    public static final String JPN_DEFAULT_FONT = "font/jpn/NotoSansCJKjp-Regular.otf";
    public static final String JPN_BOLD_FONT = "font/jpn/NotoSansCJKjp-Bold.otf";
    public static final String JPN_ITALIC_FONT = "font/jpn/NotoSansCJKjp-Medium.otf";
    public static final String KOR_DEFAULT_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
    public static final String KOR_BOLD_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
    public static final String KOR_ITALIC_FONT = "font/kor/GyeonggiCheonnyeonBatangBold.ttf";
    public static final String RUS_DEFAULT_FONT = "font/rus/FiraSansExtraCondensed-Regular.ttf";
    public static final String RUS_BOLD_FONT = "font/rus/FiraSansExtraCondensed-Bold.ttf";
    public static final String RUS_ITALIC_FONT = "font/rus/FiraSansExtraCondensed-Italic.ttf";
    public static final String SRB_DEFAULT_FONT = "font/srb/InfluBG.otf";
    public static final String SRB_BOLD_FONT = "font/srb/InfluBG-Bold.otf";
    public static final String SRB_ITALIC_FONT = "font/srb/InfluBG-Italic.otf";
    public static final String THA_DEFAULT_FONT = "font/tha/CSChatThaiUI.ttf";
    public static final String THA_BOLD_FONT = "font/tha/CSChatThaiUI.ttf";
    public static final String THA_ITALIC_FONT = "font/tha/CSChatThaiUI.ttf";
    public static final String VIE_DEFAULT_FONT = "font/vie/Grenze-Regular.ttf";
    public static final String VIE_BOLD_FONT = "font/vie/Grenze-SemiBold.ttf";
    public static final String VIE_DRAMATIC_FONT = "font/vie/Grenze-Black.ttf";
    public static final String VIE_ITALIC_FONT = "font/vie/Grenze-RegularItalic.ttf";

    private static final GlyphLayout glyph_layout = new GlyphLayout();

    public static float get_text_width(BitmapFont font, String text)  {
        glyph_layout.setText(font, text);
        return glyph_layout.width;
    }

    public static String get_cutoff_text(String text, BitmapFont font, float max_width) {
        glyph_layout.setText(font, text);
        if (get_text_width(font, text) <= max_width) return text;

        max_width -= get_text_width(font, TEXT_CUTOFF);

        // Binary search for the right width
        int min = 0;
        int max = text.length() - 1;
        while (min <= max) {
            int middle = (min + max) / 2;

            if (get_text_width(font, text.substring(0, middle)) <= max_width) {
                min = middle + 1;
            } else {
                max = middle - 1;
            }
        }

        return text.substring(0, max) + TEXT_CUTOFF;
    }

    public static String get_default_font() {
        switch (Settings.language) {
            case ZHS: return ZHS_DEFAULT_FONT;
            case ZHT: return ZHT_DEFAULT_FONT;
            case EPO: return EPO_DEFAULT_FONT;
            case GRE: return GRE_DEFAULT_FONT;
            case JPN: return JPN_DEFAULT_FONT;
            case KOR: return KOR_DEFAULT_FONT;
            case POL:
            case RUS:
            case UKR: return RUS_DEFAULT_FONT;
            case SRP:
            case SRB: return SRB_DEFAULT_FONT;
            case THA: return THA_DEFAULT_FONT;
            case VIE: return VIE_DEFAULT_FONT;
            default:  return ENG_DEFAULT_FONT;
        }
    }

    public static String get_bold_font() {
        switch (Settings.language) {
            case ZHS: return ZHS_BOLD_FONT;
            case ZHT: return ZHT_BOLD_FONT;
            case EPO: return EPO_BOLD_FONT;
            case GRE: return GRE_BOLD_FONT;
            case JPN: return JPN_BOLD_FONT;
            case KOR: return KOR_BOLD_FONT;
            case POL:
            case RUS:
            case UKR: return RUS_BOLD_FONT;
            case SRP:
            case SRB: return SRB_BOLD_FONT;
            case THA: return THA_BOLD_FONT;
            case VIE: return VIE_BOLD_FONT;
            default:  return ENG_BOLD_FONT;
        }
    }

    public static String get_italic_font() {
        switch (Settings.language) {
            case ZHS: return ZHS_ITALIC_FONT;
            case ZHT: return ZHT_ITALIC_FONT;
            case EPO: return EPO_ITALIC_FONT;
            case GRE: return GRE_ITALIC_FONT;
            case JPN: return JPN_ITALIC_FONT;
            case KOR: return KOR_ITALIC_FONT;
            case POL:
            case RUS:
            case UKR: return RUS_ITALIC_FONT;
            case SRP:
            case SRB: return SRB_ITALIC_FONT;
            case THA: return THA_ITALIC_FONT;
            case VIE: return VIE_ITALIC_FONT;
            default:  return ENG_ITALIC_FONT;
        }
    }

    public static String get_dramatic_font() {
        switch (Settings.language) {
            case VIE: return VIE_DRAMATIC_FONT;
            default:  return ENG_DRAMATIC_FONT;
        }
    }

    public static String get_pixel_font() {
        return TINY_NUMBERS_FONT;
    }

    public static BitmapFont prep_font(String font_file_path,
                                       float size, boolean linear_filtering, float gamma,
                                       int space_x, int space_y,
                                       Color border_color, boolean border_straight, float border_width, float border_gamma,
                                       Color shadow_color, int shadow_offset_x, int shadow_offset_y) {
        HashMap<String, FreeTypeFontGenerator> generators = ReflectionHacks.getPrivateStatic(FontHelper.class, "generators");
        FreeTypeFontGenerator g;
        if (generators.containsKey(font_file_path)) {
            g = generators.get(font_file_path);
        } else {
            g = new FreeTypeFontGenerator(Gdx.files.internal(font_file_path));
            generators.put(font_file_path, g);
        }

        if (Settings.BIG_TEXT_MODE) {
            size *= 1.2f;
        }

        return prep_font(g, font_file_path,
                size, linear_filtering, gamma,
                space_x, space_y,
                border_color, border_straight, border_width, border_gamma,
                shadow_color, shadow_offset_x, shadow_offset_y);
    }

    private static BitmapFont prep_font(FreeTypeFontGenerator g, String font_file_path,
                                        float size, boolean linear_filtering, float gamma,
                                        int space_x, int space_y,
                                        Color border_color, boolean border_straight, float border_width, float border_gamma,
                                        Color shadow_color, int shadow_offset_x, int shadow_offset_y) {
        FreeTypeFontGenerator.FreeTypeFontParameter p = new FreeTypeFontGenerator.FreeTypeFontParameter();
        p.characters = "";
        p.incremental = true;
        p.size = Math.round(size * Settings.scale);
        p.gamma = gamma;
        p.spaceX = space_x;
        p.spaceY = space_y;
        p.borderColor = border_color;
        p.borderStraight = border_straight;
        p.borderWidth = border_width;
        p.borderGamma = border_gamma;
        p.shadowColor = shadow_color;
        p.shadowOffsetX = shadow_offset_x;
        p.shadowOffsetY = shadow_offset_y;
        if (linear_filtering) {
            p.minFilter = Texture.TextureFilter.Linear;
            p.magFilter = Texture.TextureFilter.Linear;
        } else {
            p.minFilter = Texture.TextureFilter.Nearest;
            p.magFilter = Texture.TextureFilter.MipMapLinearNearest;
        }

        g.scaleForPixelHeight(p.size);
        BitmapFont font = g.generateFont(p);
        font.setUseIntegerPositions(!linear_filtering);
        font.getData().markupEnabled = true;
        if (LocalizedStrings.break_chars != null) {
            font.getData().breakChars = LocalizedStrings.break_chars.toCharArray();
        }

        font.getData().fontFile = Gdx.files.internal(font_file_path);
        return font;
    }
}
