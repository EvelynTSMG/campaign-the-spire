package EvesActMod.util;

import EvesActMod.EvesActMod;
import EvesActMod.common.Option;
import basemod.BaseMod;
import basemod.ReflectionHacks;
import basemod.abstracts.CustomCard;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.GLFrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.TipHelper;
import com.megacrit.cardcrawl.helpers.input.InputHelper;
import jdk.internal.vm.annotation.ForceInline;

import java.util.ArrayList;
import java.util.function.Consumer;

import static EvesActMod.EvesActMod.image_path;
import static EvesActMod.common.Option.Some;

@SuppressWarnings("unused")
public class ImageHelper {
    public static final float MAX_SCREEN_SIZE = Math.max(Settings.WIDTH, Settings.HEIGHT);
    public static final float PIXEL_SNAP = scaled(0.5f);
    public static final float CARD_ART_WIDTH = 500;
    public static final float CARD_ART_HEIGHT = 380;

    public static final OrthographicCamera screen_cam = new OrthographicCamera(Settings.WIDTH, Settings.HEIGHT);

    private static final Rectangle screen_rectangle = new Rectangle(0, 0, Settings.WIDTH, Settings.HEIGHT);
    private static final ArrayList<Rectangle> scissor_stack = new ArrayList<>();
    private static final FrameBuffer card_art_buffer = ImageHelper.create_buffer(CARD_ART_WIDTH, CARD_ART_HEIGHT);
    private static final OrthographicCamera card_camera = new OrthographicCamera(CARD_ART_WIDTH, CARD_ART_HEIGHT);
    private static final ShapeRenderer shape_renderer = new ShapeRenderer();
    private static final SpriteBatch burner_sb = new SpriteBatch();

    public static abstract class Anchor {
        public static int TOP = 1;
        public static int RIGHT = 1 << 1;
        public static int BOTTOM = 1 << 2;
        public static int LEFT = 1 << 3;

        public static Hitbox adjust(int anchor, Hitbox hb) {
            float x = hb.x;
            if ((anchor & LEFT) == 0) x += hb.width/2f;
            if ((anchor & RIGHT) != 0) x += hb.width/2f;

            float y = hb.y;
            if ((anchor & BOTTOM) == 0) y += hb.height/2f;
            if ((anchor & TOP) != 0) y += hb.height/2f;

            return new Hitbox(x, y, hb.width, hb.height);
        }

        public static Hitbox adjust(int from_anchor, int to_anchor, Hitbox hb) {
            float x = hb.x;
            if ((to_anchor & LEFT) - (from_anchor & LEFT) != 0) x -= hb.width/2f;
            if ((to_anchor & RIGHT) - (from_anchor & RIGHT) != 0) x += hb.width/2f;

            float y = hb.y;
            if ((to_anchor & BOTTOM) - (from_anchor & BOTTOM) != 0) y -= hb.width/2f;
            if ((to_anchor & TOP) - (from_anchor & BOTTOM) != 0) y += hb.width/2f;

            return new Hitbox(x, y, hb.width, hb.height);
        }
    }

    @ForceInline
    public static float scaled(float n) {
        return n * Settings.scale;
    }

    @ForceInline
    public static Texture texture_at(String path) {
        return new Texture(Gdx.files.internal(image_path(path)));
    }

    public static void clear_screen(Option<SpriteBatch> sb, Color c) {
        boolean restart = sb.is_some_and(SpriteBatch::isDrawing);
        if (restart) sb.inspect(SpriteBatch::end);
        Gdx.gl.glClearColor(c.r, c.g, c.b, c.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        if (restart) sb.inspect(SpriteBatch::begin);
    }

    public static FrameBuffer create_buffer() {
        return create_buffer(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
    }

    public static FrameBuffer create_buffer(int width, int height) {
        return new FrameBuffer(Pixmap.Format.RGBA8888, width, height, false, false);
    }

    public static FrameBuffer create_buffer(float width, float height) {
        return new FrameBuffer(Pixmap.Format.RGBA8888, (int)Math.ceil(width), (int)Math.ceil(height), false, false);
    }

    public static void begin_buffer(FrameBuffer fb) {
        fb.begin();
        Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 0.0F);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glColorMask(true, true, true, true);
    }

    public static TextureRegion buffer_texture(FrameBuffer fbo) {
        TextureRegion texture = new TextureRegion(fbo.getColorBufferTexture());
        texture.flip(false, true);
        return texture;
    }

    public static TextureAtlas.AtlasRegion as_atlas_region(Texture tex) {
        return new TextureAtlas.AtlasRegion(tex, 0, 0, tex.getWidth(), tex.getHeight());
    }

    public static void draw_scaled(SpriteBatch sb, Texture tex, float x, float y) {
        sb.draw(tex,
                x, y,
                0, 0,
                scaled(tex.getWidth()), scaled(tex.getHeight()),
                1, 1,
                0,
                0, 0,
                tex.getWidth(), tex.getHeight(),
                false, false);
    }

    public static void draw_centered(SpriteBatch sb, TextureRegion tex, float x, float y, float scale) {
        draw_centered(sb, tex, x, y, scale, scale);
    }

    public static void draw_centered(SpriteBatch sb, TextureRegion tex, float x, float y, float scale_x, float scale_y) {
        float width = tex.getRegionWidth();
        float height = tex.getRegionHeight();
        sb.draw(tex,
            x - width / 2f, y - height / 2f,
            width / 2f, height / 2f,
            width, height,
            scale_x * Settings.scale, scale_y * Settings.scale,
            0);
    }

    public static void draw_centered(SpriteBatch sb, Texture tex, float x, float y, float scale) {
        draw_centered(sb, tex, x, y, scale, scale);
    }

    public static void draw_centered(SpriteBatch sb, Texture tex, float x, float y, float scale_x, float scale_y) {
        int width = tex.getWidth();
        int height = tex.getHeight();
        sb.draw(tex,
            x - width / 2f, y - height / 2f,
            width / 2f, height / 2f,
            width, height,
            scale_x * Settings.scale, scale_y * Settings.scale, 0,
            0, 0,
            width, height,
            false, false);
    }

    public static void tip_box_at_cursor(String name, String description) {
        if ((float) InputHelper.mX < scaled(1400f)) {
            TipHelper.renderGenericTip(
                    (float) InputHelper.mX + scaled(60f), (float) InputHelper.mY - scaled(50f),
                    name,
                    description);
        } else {
            TipHelper.renderGenericTip((float) InputHelper.mX - scaled(350f), (float) InputHelper.mY - scaled(50f),
                    name,
                    description);
        }
    }

    public static void clear_current_buffer() {
        Gdx.gl20.glClearColor(0, 0, 0, 0);
        Gdx.gl20.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        Gdx.gl20.glColorMask(true, true, true, true);
    }

    public static void swap_color_texture(FrameBuffer fb, boolean is_buffer_round) {
        Pixmap.Format format = ReflectionHacks.getPrivate(fb, GLFrameBuffer.class, "format");
        Texture tex = new Texture(fb.getWidth(), fb.getHeight(), format);
        swap_color_texture(fb, tex, is_buffer_round);
    }

    public static void swap_color_texture(FrameBuffer fb, Texture tex, boolean is_buffer_bound) {
        ReflectionHacks.setPrivate(fb, GLFrameBuffer.class, "colorTexture", tex);
        if (!is_buffer_bound) fb.begin();
        Gdx.gl20.glBindTexture(tex.glTarget, tex.getTextureObjectHandle());
        Gdx.gl20.glFramebufferTexture2D(36160, 36064, 3553, tex.getTextureObjectHandle(), 0);
        Gdx.gl20.glBindTexture(tex.glTarget, 0);
        if (!is_buffer_bound) fb.end();
    }

    public static void swap_texture_and_clear(FrameBuffer fb) {
        swap_color_texture(fb, true);
        clear_current_buffer();
    }

    public static void swap_texture_and_clear(FrameBuffer fb, Texture tex) {
        swap_color_texture(fb, tex, true);
        clear_current_buffer();
    }

    public static Option<Texture> get_character_portrait(AbstractPlayer.PlayerClass cls) {
        switch (cls) {
            case IRONCLAD: return Some(ImageMaster.CHAR_SELECT_BG_IRONCLAD);
            case THE_SILENT: return Some(ImageMaster.CHAR_SELECT_BG_SILENT);
            case DEFECT: return Some(ImageMaster.CHAR_SELECT_BG_DEFECT);
            case WATCHER: return Some(ImageMaster.CHAR_SELECT_BG_WATCHER);
            default: return Option.from(ImageMaster.loadImage(BaseMod.getPlayerPortrait(cls)));
        }
    }

    public static Texture make_rectangle_portrait(AbstractCard card, Option<Texture> fade_mask) {
        card.update();
        TextureAtlas.AtlasRegion t = new TextureAtlas.AtlasRegion(card.portrait);

        card_camera.position.setZero();
        if (card.type == AbstractCard.CardType.ATTACK) {
            card_camera.zoom = 0.9f;
            card_camera.translate(0, -20);
        } else if (card.type == AbstractCard.CardType.POWER) {
            card_camera.zoom = 0.825f;
            card_camera.translate(-2, -36);
        }

        SpriteBatch sb = new SpriteBatch();
        sb.setProjectionMatrix(card_camera.combined);

        t.flip(false, true);
        card_art_buffer.begin();
        swap_texture_and_clear(card_art_buffer);
        sb.begin();
        sb.draw(t, -CARD_ART_WIDTH/2f, -CARD_ART_HEIGHT/2f);

        fade_mask.inspect(mask -> {
            sb.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
            sb.setProjectionMatrix(new OrthographicCamera(CARD_ART_WIDTH, CARD_ART_HEIGHT).combined);
            sb.draw(mask, -CARD_ART_WIDTH/2f, -CARD_ART_HEIGHT/2f,
                    -CARD_ART_WIDTH/2f, -CARD_ART_HEIGHT/2f,
                    CARD_ART_WIDTH, CARD_ART_HEIGHT,
                    1, 1, 0,
                    0, 0,
                    mask.getWidth(), mask.getHeight(),
                    false, true);
        });

        sb.end();
        card_art_buffer.end();
        t.flip(false, true);

        TextureRegion a = ImageHelper.buffer_texture(card_art_buffer);
        return a.getTexture();
    }

    public static TextureAtlas.AtlasRegion get_energy_icon(AbstractCard card) {
        TextureAtlas.AtlasRegion orb_tex = ImageMaster.CARD_GRAY_ORB_L;
        if (card instanceof CustomCard) {
            Texture tex = ((CustomCard)card).getOrbLargeTexture();
            if (tex == null) {
                tex = BaseMod.getEnergyOrbPortraitTexture(card.color);
                if (tex == null) {
                    tex = ImageMaster.loadImage(BaseMod.getEnergyOrbPortrait(card.color));
                    BaseMod.saveEnergyOrbPortraitTexture(card.color, tex);
                }
            }
            if (tex != null) {
                orb_tex = new TextureAtlas.AtlasRegion(tex, 0, 0, tex.getWidth(), tex.getHeight());
            } else {
                EvesActMod.logger.info("Modded energy icon is null for color `" + card.color.name() + "`");
            }
        } else {
            switch (card.color) {
                case RED:
                    orb_tex = ImageMaster.CARD_RED_ORB_L;
                    break;
                case GREEN:
                    orb_tex = ImageMaster.CARD_GREEN_ORB_L;
                    break;
                case BLUE:
                    orb_tex = ImageMaster.CARD_BLUE_ORB_L;
                    break;
                case PURPLE:
                    orb_tex = ImageMaster.CARD_PURPLE_ORB_L;
                    break;
            }
        }

        return orb_tex;
    }

    private static Rectangle get_scissors_rect() {
        return scissor_stack.stream().reduce(screen_rectangle, (a, b) -> {
            float left = Math.max(a.x, b.x);
            float bottom = Math.max(a.y, b.y);
            float right = Math.min(a.x + a.width, b.x + b.width);
            float top = Math.min(a.y + a.height, b.y + b.height);
            return new Rectangle(left, bottom, right - left, top - bottom);
        });
    }

    private static void glScissor(Rectangle rect) {
        Gdx.gl.glScissor((int)Math.floor(rect.x), (int)Math.floor(rect.y),
                (int)Math.ceil(rect.width), (int)Math.ceil(rect.height));
    }

    public static void scissors(SpriteBatch sb, AdvancedHitbox hb, Runnable action) {
        scissors(sb, hb.left(), hb.bottom(), hb.width(), hb.height(), action);
    }

    public static void scissors(SpriteBatch sb, float x, float y, float width, float height, Runnable action) {
        Rectangle scissors = new Rectangle();
        scissor_stack.add(new Rectangle(x, y, width, height));
        sb.flush();

        boolean was_enabled = Gdx.gl.glIsEnabled(GL20.GL_SCISSOR_TEST);
        if (!was_enabled) Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
        glScissor(get_scissors_rect());

        action.run();
        sb.flush();
        scissor_stack.remove(scissor_stack.size() - 1);
        if (!was_enabled) Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);
    }

    public static void ignore_scissors(SpriteBatch sb, Runnable action) {
        boolean was_enabled = Gdx.gl.glIsEnabled(GL20.GL_SCISSOR_TEST);
        sb.flush();
        Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST);
        action.run();
        sb.flush();
        if (was_enabled) {
            Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST);
            glScissor(get_scissors_rect());
        }
    }

    @Deprecated
    public static void depth_mask(SpriteBatch sb, Consumer<ShapeRenderer> render_mask, Runnable render_masked) {
        sb.end();

        Gdx.gl.glClearDepthf(1f);
        Gdx.gl.glClear(GL20.GL_DEPTH_BUFFER_BIT);

        Gdx.gl.glDepthFunc(GL20.GL_LESS);

        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);

        Gdx.gl.glDepthMask(true);
        Gdx.gl.glColorMask(false, false, false, false);

        render_mask.accept(shape_renderer);

        sb.begin();
        Gdx.gl.glColorMask(true, true, true, true);
        Gdx.gl.glEnable(GL20.GL_DEPTH_TEST);
        Gdx.gl.glDepthFunc(GL20.GL_EQUAL);

        render_masked.run();
        sb.flush();

        Gdx.gl.glDisable(GL20.GL_DEPTH_TEST);
    }

    @Deprecated
    public static void blend_mask(SpriteBatch sb, Runnable render_mask, Runnable render_masked) {
        sb.end();
        sb.begin();
        render_masked.run();
        sb.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
        render_mask.run();
        sb.setBlendFunction(GL20.GL_ONE, GL20.GL_ONE);
        sb.end();
        sb.begin();
    }

    public static Color get_rarity_color(AbstractCard.CardRarity rarity) {
        switch (rarity) {
            case BASIC: return Color.LIGHT_GRAY;
            case COMMON: return Color.WHITE;
            case UNCOMMON: return Color.CYAN;
            case RARE: return Color.GOLD;
            case SPECIAL: return Color.GREEN;
            case CURSE: return Color.DARK_GRAY;
        }

        // Someone @SpireEnum'd the bitch
        return Color.WHITE;
    }
}
