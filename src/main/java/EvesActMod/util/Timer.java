package EvesActMod.util;

import com.badlogic.gdx.Gdx;

/**
 * A countdown timer.<br>
 * Counts down from a specified {@code wait_time} by the last delta time whenever {@code tick} is called.
 */
public class Timer {
    /**
     * If {@code false}, the timer will stop when reaching 0. If {@code true}, it will restart.
     */
    public boolean repeat;
    /**
     * If {@code true}, the timer will not tick until it is unpaused again, even if {@code start} is called
     */
    public boolean paused;
    private float time_left;
    private float wait_time;

    /**
     * @param wait_time The wait time in seconds.
     * @param autostart If {@code true}, the timer will start automatically when constructed.
     * @param repeat If {@code true}, the timer will restart when reaching 0. Else, it will stop.
     */
    public Timer(float wait_time, boolean autostart, boolean repeat) {
        this.wait_time = wait_time;
        if (autostart) time_left = wait_time;
        this.repeat = repeat;
    }

    public boolean is_stopped() {
        return time_left == 0;
    }

    public float get_percentage() {
        return time_left / wait_time;
    }

    public float get_time_left() {
        return time_left;
    }

    /**
     * Ticks down the timer by delta time.
     * @return How much time is left
     */
    public float tick() {
        if (paused || time_left == 0) return time_left;

        time_left -= Gdx.graphics.getDeltaTime();
        if (time_left <= 0) {
            if (repeat) time_left = wait_time;
            else time_left = 0;

            return 0;
        }

        return time_left;
    }

    /**
     * Restarts the timer. Sets {@code time_left} to the last {@code wait_time}.<br>
     * <b>Note:</b> This will not resume a paused timer.
     *
     * @see Timer#paused
     */
    public void start() {
        time_left = wait_time;
    }

    /**
     * Starts the timer. Sets {@code wait_time} to {@code time_sec} if {@code time_sec > 0}. This also restarts the timer.<br>
     * <b>Note:</b> This will not resume a paused timer.
     *
     * @param time_sec The time to wait for, in seconds.
     *
     * @see Timer#paused
     */
    public void start(float time_sec) {
        if (time_sec <= 0) return;
        time_left = wait_time = time_sec;
    }

    /**
     * Stops the timer.
     */
    public void stop() {
        time_left = 0;
    }
}
