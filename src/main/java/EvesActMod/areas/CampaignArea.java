package EvesActMod.areas;

import EvesActMod.EvesActMod;
import EvesActMod.common.Option;
import com.megacrit.cardcrawl.helpers.Hitbox;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static EvesActMod.EvesActMod.PLACEHOLDER_TEXT;
import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;

public class CampaignArea {
    public String id;
    public Hitbox hitbox;
    public AreaStrings text;
    public ArrayList<CampaignArea> neighbors;

    public CampaignArea(String id, float x, float y, float width, float height) {
        hitbox = new Hitbox(x, y, width, height);
        this.id = id;
        text = AreaStrings.get(id).unwrap_or(AreaStrings.DEFAULT);
    }

    public static class AreaStrings {
        private static final Map<String, AreaStrings> AREA_STRINGS = new HashMap<>();
        public static final AreaStrings DEFAULT = new AreaStrings(PLACEHOLDER_TEXT, PLACEHOLDER_TEXT);

        public String NAME;
        public String DESCRIPTION;

        private AreaStrings(String name, String description) {
            NAME = name;
            DESCRIPTION = description;
        }

        public static Option<AreaStrings> get(String id) {
            if (AREA_STRINGS.containsKey(id)) {
                return Some(AREA_STRINGS.get(id));
            }

            return None();
        }

        public static void add(String id, AreaStrings strings) {
            AREA_STRINGS.put(id, strings);
        }

        public static void clear() {
            AREA_STRINGS.clear();
        }

        public static void log() {
            AREA_STRINGS.forEach((id, strings) -> {
                EvesActMod.logger.info(String.format("%s - %s (%s)", id, strings.NAME, strings.DESCRIPTION));
            });
        }
    }
}
