package EvesActMod;

import EvesActMod.areas.CampaignArea;
import EvesActMod.cards.CampaignDeck;
import EvesActMod.common.Option;
import EvesActMod.ui.deckbuilder.CardsScreen;
import EvesActMod.ui.deckbuilder.DecksScreen;
import EvesActMod.ui.screens.CampaignDeckBuilderScreen;
import EvesActMod.ui.screens.CampaignMapScreen;
import EvesActMod.ui.screens.CampaignScreen;
import EvesActMod.ui.debug.DebugOverlay;
import EvesActMod.util.ImageShaders;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CardCampaignGame {
    public static OrthographicCamera screen_cam = new OrthographicCamera(Settings.WIDTH, Settings.HEIGHT);
    public static ArrayList<CampaignArea> areas;
    public static CampaignGameScreen current_screen;

    public static ArrayList<AbstractPlayer> characters = CardCrawlGame.characterManager.getAllCharacters();
    public static AbstractPlayer player;

    public static Map<AbstractPlayer.PlayerClass, ArrayList<CampaignDeck>> decks = new HashMap<>();
    public static CampaignDeck deck;

    public enum CampaignGameScreen {
        MAP(new CampaignMapScreen()),
        DECK_BUILDER(new CampaignDeckBuilderScreen());

        private final CampaignScreen screen;

        CampaignGameScreen(CampaignScreen screen) {
            this.screen = screen;
            init();
        }

        public void update() { screen.update(); }
        public void render(SpriteBatch sb) { screen.render(sb); }
        public void init() { screen.init(); }
        public void on_open() { screen.on_open(); }
        public void on_close() { screen.on_close(); }
    }

    public static void init() {
        characters.forEach(c -> decks.put(c.chosenClass, new ArrayList<>()));
        characters.forEach(c -> {
            CampaignDeck deck = CampaignDeck.from_starter_deck(c);
            decks.get(c.chosenClass).add(deck);
        });
        change_character(AbstractPlayer.PlayerClass.IRONCLAD);

        current_screen = CampaignGameScreen.MAP;
        current_screen.on_open();
    }

    public static void change_screen(CampaignGameScreen screen) {
        current_screen.on_close();
        current_screen = screen;
        current_screen.on_open();
    }

    public static AbstractPlayer get_character(AbstractPlayer.PlayerClass cls) {
        return Option.from(characters.stream().filter(p -> p.chosenClass == cls).findAny())
            .expect("called `CardCampaignGame::change_character` with nonexistent player class");
    }

    public static void change_character(AbstractPlayer.PlayerClass cls) {
        player = Option.from(characters.stream().filter(p -> p.chosenClass == cls).findAny())
                .expect("called `CardCampaignGame::change_character` with nonexistent player class");
        DecksScreen.populate_inline_decks();
        CardsScreen.populate_card_grid();
    }

    public static void update() {
        current_screen.update();
        DebugOverlay.update();
    }

    public static void render(SpriteBatch sb) {
        current_screen.render(sb);
        DebugOverlay.render(sb);
    }
}
