package EvesActMod.patches;

import EvesActMod.areas.CampaignArea;
import com.evacipated.cardcrawl.modthespire.lib.ByRef;
import com.evacipated.cardcrawl.modthespire.lib.LineFinder;
import com.evacipated.cardcrawl.modthespire.lib.Matcher;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertLocator;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.evacipated.cardcrawl.modthespire.lib.SpirePrefixPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireReturn;
import com.evacipated.cardcrawl.modthespire.patcher.PatchingException;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.screens.mainMenu.MainMenuScreen;
import com.megacrit.cardcrawl.screens.mainMenu.MenuButton;
import javassist.CannotCompileException;
import javassist.CtBehavior;

import java.util.ArrayList;

import static EvesActMod.EvesActMod.id;

@SuppressWarnings("unused")
public class MainMenuPatches {
    public static class ClickResult {
        @SpireEnum
        public static MenuButton.ClickResult CAMPAIGN;
    }

    public static class Text {
        private static final String[] TEXT = CardCrawlGame.languagePack.getUIString(id("MainMenuText")).TEXT;
        public static String CAMPAIGN = TEXT[0];
    }

    @SpirePatch2(clz = MainMenuScreen.class, method = "setMainMenuButtons")
    public static class AddMenuButton {
        @SpireInsertPatch(locator = Locator.class, localvars = { "index" })
        public static void setMainMenuButtons(MainMenuScreen __instance, @ByRef int[] index) {
            __instance.buttons.add(new MenuButton(MainMenuPatches.ClickResult.CAMPAIGN, index[0]++));
        }

        private static class Locator extends SpireInsertLocator {
            public int[] Locate(CtBehavior method) throws CannotCompileException, PatchingException {
                Matcher matcher = new Matcher.FieldAccessMatcher(CardCrawlGame.class, "characterManager");
                return LineFinder.findInOrder(method, new ArrayList<>(), matcher);
            }
        }
    }

    @SpirePatch2(clz = MenuButton.class, method = "setLabel")
    public static class AddMenuButtonLabel {
        @SpirePrefixPatch
        public static SpireReturn<Void> setLabel(MenuButton __instance, @ByRef String[] ___label) {
            if (__instance.result == MainMenuPatches.ClickResult.CAMPAIGN) {
                ___label[0] = MainMenuPatches.Text.CAMPAIGN;
                return SpireReturn.Return();
            }

            return SpireReturn.Continue();
        }
    }

    @SpirePatch2(clz = MenuButton.class, method = "buttonEffect")
    public static class AddMenuButtonFunctionality {
        @SpirePrefixPatch
        public static SpireReturn<Void> buttonEffect(MenuButton __instance) {
            if (__instance.result == MainMenuPatches.ClickResult.CAMPAIGN) {
                CardCrawlGame.mode = CardCrawlGame.GameMode.GAMEPLAY;
                CardCrawlGamePatches.load_campaign = true;
                return SpireReturn.Return();
            }

            return SpireReturn.Continue();
        }
    }
}
