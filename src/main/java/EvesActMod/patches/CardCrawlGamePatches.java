package EvesActMod.patches;

import EvesActMod.CardCampaignGame;
import EvesActMod.common.Option;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.evacipated.cardcrawl.modthespire.lib.LineFinder;
import com.evacipated.cardcrawl.modthespire.lib.Matcher;
import com.evacipated.cardcrawl.modthespire.lib.SpireEnum;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertLocator;
import com.evacipated.cardcrawl.modthespire.lib.SpireInsertPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpireInstrumentPatch;
import com.evacipated.cardcrawl.modthespire.lib.SpirePatch2;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.helpers.DrawMaster;
import com.megacrit.cardcrawl.screens.DungeonTransitionScreen;
import javassist.CannotCompileException;
import javassist.CtBehavior;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;
import org.apache.logging.log4j.Logger;

public class CardCrawlGamePatches {
    public static boolean load_campaign;

    public static class GameMode {
        @SpireEnum
        public static CardCrawlGame.GameMode CAMPAIGN_GAMEPLAY;
    }

    @SpirePatch2(clz = CardCrawlGame.class, method = "update")
    public static class UpdateCampaign {
        @SpireInsertPatch(locator = Locator.class)
        public static void update(CardCrawlGame __instance) {
            if (CardCrawlGame.mode == CardCrawlGame.GameMode.GAMEPLAY && load_campaign) {
                CardCrawlGame.mode = GameMode.CAMPAIGN_GAMEPLAY;

                if (CardCrawlGame.dungeonTransitionScreen != null) {
                    CardCrawlGame.dungeonTransitionScreen.isComplete = true;
                    CardCrawlGame.dungeonTransitionScreen = null;
                }

                load_campaign = false;
            } else if (CardCrawlGame.mode == GameMode.CAMPAIGN_GAMEPLAY) {
                CardCampaignGame.update();
            }
        }

        private static class Locator extends SpireInsertLocator {
            @Override
            public int[] Locate(CtBehavior behavior) throws Exception {
                Matcher matcher = new Matcher.MethodCallMatcher(CardCrawlGame.class, "updateDebugSwitch");
                return LineFinder.findInOrder(behavior, matcher);
            }
        }
    }

    @SpirePatch2(clz = CardCrawlGame.class, method = "render")
    public static class RenderCampaign {
        @SpireInsertPatch(locator = Locator.class)
        public static void render(SpriteBatch ___sb) {
            if (CardCrawlGame.mode == GameMode.CAMPAIGN_GAMEPLAY) {
                if (CardCrawlGame.dungeonTransitionScreen != null) {
                    CardCrawlGame.dungeonTransitionScreen.render(___sb);
                } else {
                    CardCampaignGame.render(___sb);
                }
            }
        }

        private static class Locator extends SpireInsertLocator {
            @Override
            public int[] Locate(CtBehavior behavior) throws Exception {
                Matcher matcher = new Matcher.MethodCallMatcher(DrawMaster.class, "draw");
                return LineFinder.findInOrder(behavior, matcher);
            }
        }
    }

    // Stop the game from logging "Unknown Game Mode" every fucken frame
    public static boolean should_log() {
        return CardCrawlGame.mode != GameMode.CAMPAIGN_GAMEPLAY;
    }

    @SpirePatch2(clz = CardCrawlGame.class, method = "update")
    public static class StopUpdateLogs {
        static int hits = 0;
        @SpireInstrumentPatch
        public static ExprEditor patch() {
            return new ExprEditor() {
                @Override
                public void edit(MethodCall m) throws CannotCompileException {
                    if (m.getClassName().equals(Logger.class.getName()) && m.getMethodName().equals("info")) {
                        if (++hits == 3) {
                            m.replace("if (EvesActMod.patches.CardCrawlGamePatches.should_log()) { $proceed($$); }");
                        }
                    }
                }
            };
        }
    }

    @SpirePatch2(clz = CardCrawlGame.class, method = "render")
    public static class StopRenderLogs {
        static int hits = 0;
        @SpireInstrumentPatch
        public static ExprEditor patch() {
            return new ExprEditor() {
                @Override
                public void edit(MethodCall m) throws CannotCompileException {
                    if (m.getClassName().equals(Logger.class.getName()) && m.getMethodName().equals("info")) {
                        if (++hits == 1) {
                            m.replace("if (EvesActMod.patches.CardCrawlGamePatches.should_log()) { $proceed($$); }");
                        }
                    }
                }
            };
        }
    }
}
