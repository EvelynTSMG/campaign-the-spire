package EvesActMod.ui.screens;

import EvesActMod.ui.Renderable;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class CampaignScreen implements Renderable {
    public abstract void update();
    public abstract void render(SpriteBatch sb);
    public void init() { }
    public void on_open() { }
    public void on_close() { }
}
