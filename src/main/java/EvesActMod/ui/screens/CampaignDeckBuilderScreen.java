package EvesActMod.ui.screens;

import EvesActMod.CardCampaignGame;
import EvesActMod.common.errors.UnreachableReachedError;
import EvesActMod.ui.deckbuilder.CardsScreen;
import EvesActMod.ui.deckbuilder.DecksScreen;
import EvesActMod.ui.deckbuilder.InlineCard;
import EvesActMod.ui.input.ScrollBar;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.megacrit.cardcrawl.characters.TheSilent;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.FontHelper;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.stream.Collectors;

import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class CampaignDeckBuilderScreen extends CampaignScreen {
    public static final float LIST_TITLE_Y = Settings.HEIGHT - scaled(40);
    public static final float LIST_TOP = LIST_TITLE_Y - scaled(40);
    public static final float LIST_BOTTOM = scaled(40);
    public static final float LIST_WIDTH = scaled(384);
    public static final float LIST_HEIGHT = LIST_TOP - LIST_BOTTOM;
    public static final float LIST_X = Settings.WIDTH - LIST_WIDTH/2f - ScrollBar.WIDTH;
    public static final float LIST_Y = LIST_BOTTOM + LIST_HEIGHT/2f;

    public static DeckBuilderMode mode = DeckBuilderMode.DECKS;

    public enum DeckBuilderMode {
        DECKS(new DecksScreen()),
        CARDS(new CardsScreen());

        public final CampaignScreen screen;

        DeckBuilderMode(CampaignScreen screen) {
            this.screen = screen;
            init();
        }
        public void update() { screen.update(); }
        public void render(SpriteBatch sb) { screen.render(sb); }
        public void init() { screen.init(); }
        public void on_open() { screen.on_open(); }
        public void on_close() { screen.on_close(); }
    }

    @Override
    public void on_open() {
        mode.on_open();
    }

    @Override
    public void on_close() {
        mode.on_close();
    }

    public static void change_screen(DeckBuilderMode mode) {
        CampaignDeckBuilderScreen.mode.on_close();
        CampaignDeckBuilderScreen.mode = mode;
        CampaignDeckBuilderScreen.mode.on_open();
    }

    @Override
    public void update() {
        mode.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        ImageHelper.clear_screen(Some(sb), new Color(20f/255f, 40f/255f, 80f/255f, 1f));
        FontHelper.renderFontCentered(sb, FontHelper.dungeonTitleFont, "YOUR DAD", Settings.WIDTH/2f, Settings.HEIGHT/2f, Color.WHITE);
        mode.render(sb);
    }
}
