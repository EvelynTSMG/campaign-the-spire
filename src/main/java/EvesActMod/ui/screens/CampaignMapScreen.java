package EvesActMod.ui.screens;

import EvesActMod.CardCampaignGame;
import EvesActMod.ui.buttons.Button;
import EvesActMod.ui.buttons.IconButton;
import EvesActMod.util.ImageHelper;
import EvesActMod.util.AdvancedHitbox;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;

import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class CampaignMapScreen extends CampaignScreen {
    public static float SCREEN_EDGE_MARGIN = scaled(8);

    public static float DECK_BUTTON_SIZE = scaled(64);
    public static float DECK_BUTTON_MARGIN = scaled(8);
    public static float DECK_BUTTON_X = Settings.WIDTH - SCREEN_EDGE_MARGIN - DECK_BUTTON_MARGIN - DECK_BUTTON_SIZE/2f;
    public static float DECK_BUTTON_Y = Settings.HEIGHT - SCREEN_EDGE_MARGIN - DECK_BUTTON_MARGIN - DECK_BUTTON_SIZE/2f;

    public static float BACK_BUTTON_SIZE = scaled(64);
    public static float BACK_BUTTON_MARGIN = scaled(8);
    public static float BACK_BUTTON_X = DECK_BUTTON_X - DECK_BUTTON_SIZE/2f - DECK_BUTTON_MARGIN - BACK_BUTTON_MARGIN - BACK_BUTTON_SIZE/2f;
    public static float BACK_BUTTON_Y = DECK_BUTTON_Y;

    public Button deck_builder_button =  new IconButton(
            AdvancedHitbox.centered(DECK_BUTTON_X, DECK_BUTTON_Y, DECK_BUTTON_SIZE, DECK_BUTTON_SIZE),
            ImageMaster.DECK_ICON, None(), None()) {
        @Override
        public void on_click() {
            CardCampaignGame.change_screen(CardCampaignGame.CampaignGameScreen.DECK_BUILDER);
        }
    };

    public Button back_button =  new IconButton(
            AdvancedHitbox.centered(BACK_BUTTON_X, BACK_BUTTON_Y, BACK_BUTTON_SIZE, BACK_BUTTON_SIZE),
            ImageMaster.SETTINGS_ICON, None(), None()) {
        @Override
        public void on_click() {
            CardCrawlGame.startOver = true;
            CardCrawlGame.fadeToBlack(0.5f);
        }
    };

    @Override
    public void update() {
        back_button.update();
        deck_builder_button.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        ImageHelper.clear_screen(Some(sb), new Color(20f/255f, 40/255f, 20f/255f, 1f));
        back_button.render(sb);
        deck_builder_button.render(sb);
        FontHelper.renderFontCentered(sb, FontHelper.dungeonTitleFont, "YOUR MOM", Settings.WIDTH/2f, Settings.HEIGHT/2f, Color.WHITE);
    }
}
