package EvesActMod.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface Renderable {
    void update();
    void render(SpriteBatch sb);
}
