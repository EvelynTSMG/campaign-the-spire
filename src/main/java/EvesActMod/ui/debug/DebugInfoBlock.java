package EvesActMod.ui.debug;

import java.util.ArrayList;
import java.util.Arrays;

public class DebugInfoBlock {
    public String header;
    public ArrayList<String> lines;

    public DebugInfoBlock(String header, String... lines) {
        this.header = header;
        this.lines = new ArrayList<>(Arrays.asList(lines));
    }

    public static String header_from(Object o) {
        return String.format("%s (#0x%08x)", o.getClass().getSimpleName(), o.hashCode());
    }
}
