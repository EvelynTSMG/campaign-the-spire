package EvesActMod.ui.debug;

import EvesActMod.util.TextHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static EvesActMod.util.ImageHelper.scaled;

public class DebugOverlay {
    public static final float MARGIN = scaled(8);
    public static final float BLOCK_GAP = scaled(6);
    public static final BitmapFont DEBUG_HEADER_FONT = TextHelper.prep_font(TextHelper.get_pixel_font(), 20f, false, 0.9f,  0, 0, Color.BLACK, false, 0, 1f, Color.BLACK.cpy().mul(0), 0, 0);;
    public static final BitmapFont DEBUG_FONT = TextHelper.prep_font(TextHelper.get_pixel_font(), 16f, false, 0.9f,  0, 0, Color.BLACK, false, 0, 1f, Color.BLACK.cpy().mul(0), 0, 0);;;
    public static final Color HEADER_COLOR = Color.GOLDENROD;
    public static final Color TEXT_COLOR = Color.WHITE;

    protected static final ArrayList<AddsDebugInfo> addables = new ArrayList<>();
    protected static final ArrayList<DebugInfoBlock> info = new ArrayList<>();

    public static void register(AddsDebugInfo object) {
        addables.add(object);
    }

    public static void unregister(AddsDebugInfo object) {
        addables.remove(object);
    }

    protected static void add_generic_info() {
        String fps_fmt = "FPS: % 2d";
        if (Settings.IS_V_SYNC) fps_fmt += " (V-Sync)";

        String resolution_fmt = "Resolution: %dx%d";
        if (Settings.IS_FULLSCREEN) resolution_fmt += " (fullscreen)";
        else if (Settings.IS_W_FULLSCREEN) resolution_fmt += " (windowed fullscreen)";
        else resolution_fmt += " (windowed)";

        info.add(new DebugInfoBlock(
            "Generic Info",
            String.format(fps_fmt, Gdx.graphics.getFramesPerSecond()),
            String.format("Delta time: % 3.02fms", Gdx.graphics.getDeltaTime() * 1000),
            String.format(resolution_fmt, Settings.WIDTH, Settings.HEIGHT)
        ));
    }

    public static void update() {
        info.clear();
        add_generic_info();
        info.addAll(addables.stream().map(AddsDebugInfo::add_block).collect(Collectors.toList()));
    }

    public static void render(SpriteBatch sb) {
        if (!Settings.isDebug) return;

        float y = Settings.HEIGHT - MARGIN;
        for (DebugInfoBlock block : info) {
            FontHelper.renderFontLeftTopAligned(sb, DEBUG_HEADER_FONT, block.header, MARGIN, y, HEADER_COLOR);
            y -= DEBUG_HEADER_FONT.getLineHeight();

            for (String line : block.lines) {
                FontHelper.renderFontLeftTopAligned(sb, DEBUG_FONT, line, MARGIN, y, TEXT_COLOR);
                y -= DEBUG_FONT.getLineHeight();
            }

            y -= BLOCK_GAP;
        }
    }
}
