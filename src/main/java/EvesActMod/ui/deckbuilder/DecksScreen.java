package EvesActMod.ui.deckbuilder;

import EvesActMod.CardCampaignGame;
import EvesActMod.ui.layouts.ListLayout;
import EvesActMod.ui.layouts.PaginatedGridLayout;
import EvesActMod.ui.screens.CampaignDeckBuilderScreen;
import EvesActMod.ui.screens.CampaignScreen;
import EvesActMod.ui.buttons.IconButton;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class DecksScreen extends CampaignScreen {
    public static final float MAP_BUTTON_SIZE = scaled(64);
    public static final float MAP_BUTTON_X = CampaignDeckBuilderScreen.LIST_X - CampaignDeckBuilderScreen.LIST_WIDTH/2f - MAP_BUTTON_SIZE/2f;
    public static final float MAP_BUTTON_Y = CampaignDeckBuilderScreen.LIST_TITLE_Y;

    public static final float CHAR_GRID_MARGIN_BOTTOM = scaled(32);
    public static final float CHAR_GRID_MARGIN_LEFT = scaled(128);
    public static final float CHAR_GRID_WIDTH = scaled(1300);
    public static final float CHAR_GRID_TOP = Settings.HEIGHT - scaled(200);
    public static final float CHAR_GRID_BOTTOM = CHAR_GRID_MARGIN_BOTTOM;
    public static final float CHAR_GRID_HEIGHT = CHAR_GRID_TOP - CHAR_GRID_BOTTOM;
    public static final float CHAR_GRID_X = CHAR_GRID_MARGIN_LEFT + CHAR_GRID_WIDTH /2f;
    public static final float CHAR_GRID_Y = CHAR_GRID_BOTTOM + CHAR_GRID_HEIGHT /2f;
    public static final float CHAR_GRID_HGAP = GridCharPanel.WIDTH;
    public static final float CHAR_GRID_VGAP = GridCharPanel.HEIGHT;
    public static final int CHAR_GRID_COLUMNS = 4;
    public static final int CHAR_GRID_ROWS = 2;

    public static final Texture CHAR_GRID_OVERLAY = ImageHelper.texture_at("ui/deckbuilder/char_grid_overlay.png");

    public static final float CHAR_GRID_ARROW_SIZE = scaled(32);
    public static final float CHAR_GRID_ARROW_SCALE = 2f;
    public static final float CHAR_GRID_ARROW_RIGHT_X = CHAR_GRID_X + CHAR_GRID_WIDTH/2f + CHAR_GRID_ARROW_SIZE;
    public static final float CHAR_GRID_ARROW_LEFT_X = CHAR_GRID_X - CHAR_GRID_WIDTH/2f - CHAR_GRID_ARROW_SIZE;
    public static final float CHAR_GRID_ARROW_Y = CHAR_GRID_Y;

    public static final float DECK_LIST_GAP = InlineDeck.MARGIN * 2 + InlineDeck.HEIGHT;

    public static final IconButton map_button = new IconButton(
        AdvancedHitbox.centered(MAP_BUTTON_X, MAP_BUTTON_Y, MAP_BUTTON_SIZE, MAP_BUTTON_SIZE),
        ImageMaster.MAP_ICON, None(), None()) {
        @Override
        public void on_click() {
            CardCampaignGame.change_screen(CardCampaignGame.CampaignGameScreen.MAP);
        }
    };

    public static final ListLayout<InlineDeck> inlined_decks = new ListLayout<>(
        AdvancedHitbox.centered(CampaignDeckBuilderScreen.LIST_X, CampaignDeckBuilderScreen.LIST_Y,
            CampaignDeckBuilderScreen.LIST_WIDTH, CampaignDeckBuilderScreen.LIST_HEIGHT),
        ListLayout.ListMode.VERTICAL, DECK_LIST_GAP, 0, InlineDeck.MARGIN,
        false, false);

    public static final PaginatedGridLayout<GridCharPanel> char_grid = new PaginatedGridLayout<>(
        AdvancedHitbox.centered(CHAR_GRID_X, CHAR_GRID_Y, CHAR_GRID_WIDTH, CHAR_GRID_HEIGHT),
        CHAR_GRID_HGAP, CHAR_GRID_VGAP, CHAR_GRID_COLUMNS, CHAR_GRID_ROWS);
    public static final IconButton char_grid_arrow_left = new IconButton(
        AdvancedHitbox.centered(CHAR_GRID_ARROW_LEFT_X, CHAR_GRID_ARROW_Y, CHAR_GRID_ARROW_SIZE, CHAR_GRID_ARROW_SIZE),
        IconButton.ARROW_LEFT, Some(IconButton.ARROW_LEFT_HOVER), Some(IconButton.ARROW_LEFT)) {
        @Override
        public void on_click() {
            if (!char_grid.can_dec_page()) return;
            char_grid.dec_page();
            update_char_grid_arrows_scale();
        }
    };
    public static final IconButton char_grid_arrow_right = new IconButton(
        AdvancedHitbox.centered(CHAR_GRID_ARROW_RIGHT_X, CHAR_GRID_ARROW_Y, CHAR_GRID_ARROW_SIZE, CHAR_GRID_ARROW_SIZE),
        IconButton.ARROW_RIGHT, Some(IconButton.ARROW_RIGHT_HOVER), Some(IconButton.ARROW_RIGHT)) {
        @Override
        public void on_click() {
            if (!char_grid.can_inc_page()) return;
            char_grid.inc_page();
            update_char_grid_arrows_scale();
        }
    };

    static {
        ArrayList<AbstractPlayer.PlayerClass> a = CardCampaignGame.characters.stream().map(c -> c.chosenClass).collect(Collectors.toCollection(ArrayList::new));

        char_grid.clear();
        for (AbstractPlayer.PlayerClass cls : a) {
            char_grid.add(new GridCharPanel(cls));
        }
    }

    public static void update_char_grid_arrows_scale() {
        char_grid_arrow_left.hb.scale = char_grid.can_dec_page() ? CHAR_GRID_ARROW_SCALE : 0;
        char_grid_arrow_right.hb.scale = char_grid.can_inc_page() ? CHAR_GRID_ARROW_SCALE : 0;
    }

    public static void select_character(AbstractPlayer.PlayerClass cls) {
        CardCampaignGame.change_character(cls);
        char_grid.for_each(panel -> panel.selected = panel.cls == cls);
    }

    public static void populate_inline_decks() {
        inlined_decks.set_all(CardCampaignGame.decks.get(CardCampaignGame.player.chosenClass).stream()
            .map(InlineDeck::new)
            .collect(Collectors.toCollection(ArrayList::new)));
        inlined_decks.sort((a, b) -> a.deck.name.compareToIgnoreCase(b.deck.name));

        final float[] y = {CampaignDeckBuilderScreen.LIST_TOP - InlineDeck.HEIGHT / 2f};
        inlined_decks.for_each(deck -> {
            deck.set_pos(CampaignDeckBuilderScreen.LIST_X, y[0]);
            y[0] -= InlineDeck.MARGIN;
            y[0] -= InlineDeck.HEIGHT;
            y[0] -= InlineDeck.MARGIN;
        });
    }

    @Override
    public void on_open() {
        populate_inline_decks();
        update_char_grid_arrows_scale();
        select_character(CardCampaignGame.player.chosenClass);
    }

    @Override
    public void on_close() {
        super.on_close();
    }

    @Override
    public void update() {
        map_button.update();

        char_grid_arrow_left.update();
        char_grid_arrow_right.update();
        char_grid.update();

        inlined_decks.for_each(InlineDeck::update);
    }

    @Override
    public void render(SpriteBatch sb) {
        map_button.render(sb);

        char_grid.render(sb);
        ImageHelper.draw_centered(sb, CHAR_GRID_OVERLAY, CHAR_GRID_X, CHAR_GRID_Y, 1f);

        char_grid_arrow_left.render(sb);
        char_grid_arrow_right.render(sb);

        FontHelper.renderFontCentered(sb, FontHelper.panelNameFont, CardCampaignGame.player.getLocalizedCharacterName(), CampaignDeckBuilderScreen.LIST_X, CampaignDeckBuilderScreen.LIST_TITLE_Y);
        inlined_decks.for_each(deck -> deck.render(sb));
    }
}
