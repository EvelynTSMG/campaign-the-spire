package EvesActMod.ui.deckbuilder;

import EvesActMod.ui.Hittable;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import EvesActMod.util.TextHelper;
import EvesActMod.util.Wiz;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class InlineCard extends Hittable {
    public static final BitmapFont TITLE_FONT = TextHelper.prep_font(TextHelper.get_default_font(), 24f, false, 1f,  0, 0, Color.BLACK, false, 2f, 1f, Color.BLACK.cpy().mul(0), 0, 0);
    public static final BitmapFont AMOUNT_FONT = TextHelper.prep_font(TextHelper.get_default_font(), 28f, false, 1f,  0, 0, Color.BLACK, false, 2f, 1f, Color.BLACK.cpy().mul(0), 0, 0);
    public static final BitmapFont ENERGY_FONT = TextHelper.prep_font(TextHelper.get_bold_font(), 32f, false, 1f,  0, 0, Color.BLACK, false, 2f, 1f, Color.BLACK.cpy().mul(0), 0, 0);

    public static final float WIDTH = scaled(340);
    public static final float HEIGHT = scaled(32);

    public static final float PADDING = scaled(4);
    public static final float MARGIN = scaled(8);

    public static final float OUTSET = scaled(8);
    public static final float OUTSET_SPEED = 6f;

    public static final float ENERGY_SCALE = 0.65f/2f;
    public static final float ENERGY_PADDING = scaled(28);

    public static final float PREVIEW_PADDING = AbstractCard.RAW_H/2f;

    public static final float AMOUNT_WIDTH = scaled(32);

    private static final Texture ART_MASK = ImageHelper.texture_at("masks/InlineCardMask.png");
    public static final float ART_OFFSET = 96f;

    public static final TextureRegion BORDER = new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/inline_card_border.png"));

    public static final TextureRegion BACKGROUND = new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/inline_card_bg.png"));
    public static final float BACKGROUND_OFFSET = scaled(0.3f);

    public AdvancedHitbox hover_hb;
    public AbstractCard card;
    public TextureRegion art;
    public TextureRegion energy_icon;
    public Color border_color;
    public String title;
    public int amount = 1;

    public InlineCard(AbstractCard card) {
        hb = new AdvancedHitbox(WIDTH, HEIGHT);
        hover_hb = new AdvancedHitbox(WIDTH, HEIGHT + MARGIN * 2);
        Texture a = ImageHelper.make_rectangle_portrait(card, Some(ART_MASK));
        art = new TextureRegion(a,
                0, ART_OFFSET/ImageHelper.CARD_ART_HEIGHT,
                WIDTH/ImageHelper.CARD_ART_WIDTH, (ART_OFFSET+HEIGHT)/ImageHelper.CARD_ART_HEIGHT);
        energy_icon = new TextureRegion(ImageHelper.get_energy_icon(card));
        border_color = ImageHelper.get_rarity_color(card.rarity);

        this.card = card;
        update_title();
    }

    public void update_title() {
        title = TextHelper.get_cutoff_text(card.name, AMOUNT_FONT,
                WIDTH
                        - ENERGY_PADDING
                        - PADDING
                        - (amount > 1 ? AMOUNT_WIDTH : 0));
    }

    @Override
    public void update() {
        hb.offset_x = Wiz.lerp(hb.offset_x, hover_hb.hovered ? -OUTSET : 0, OUTSET_SPEED, ImageHelper.PIXEL_SNAP);
        hb.update();
        hover_hb.move(hb.cX, hb.cY() - MARGIN);
        hover_hb.update();


        if (hover_hb.hovered && (InputHelper.justReleasedClickLeft || InputHelper.justReleasedClickRight)) {
            CardsScreen.remove_card(card);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setColor(border_color);
        ImageHelper.draw_centered(sb, BORDER, hb.cX(), hb.cY(), 1f);
        sb.setColor(Color.WHITE);

        ImageHelper.draw_centered(sb, BACKGROUND, hb.cX(), hb.cY(), 1f);
        ImageHelper.draw_centered(sb, art, hb.left() + hb.width() * (1 - BACKGROUND_OFFSET), hb.cY(), 1f);

        if (amount != 1) {
            FontHelper.renderFontRightAligned(sb, AMOUNT_FONT, String.valueOf(amount), hb.left() + hb.width() - PADDING, hb.cY(), Color.WHITE);
        }

        FontHelper.renderFontLeft(sb, TITLE_FONT, title, hb.left() + PADDING + ENERGY_PADDING, hb.cY(), Color.WHITE);

        ImageHelper.draw_centered(sb, energy_icon, hb.left(), hb.cY(), ENERGY_SCALE);
        FontHelper.renderFontCentered(sb, ENERGY_FONT, Wiz.get_cost_text(card.cost), hb.left(), hb.cY(), Color.WHITE);

        if (hover_hb.hovered) {
            ImageHelper.ignore_scissors(sb, () -> render_preview(sb));
        }

        hb.render(sb);
    }

    private void render_preview(SpriteBatch sb) {
        AbstractCard card = this.card.makeStatEquivalentCopy();
        card.current_y = Math.max(PREVIEW_PADDING, Math.min(InputHelper.mY, Settings.HEIGHT - PREVIEW_PADDING));
        card.current_x = hb.cX - WIDTH;
        card.drawScale = 0.85f;
        card.render(sb);
    }
}
