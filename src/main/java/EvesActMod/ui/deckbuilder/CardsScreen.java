package EvesActMod.ui.deckbuilder;

import EvesActMod.CardCampaignGame;
import EvesActMod.common.Option;
import EvesActMod.common.errors.UnexpectedStateError;
import EvesActMod.ui.input.SimpleTextField;
import EvesActMod.ui.layouts.ListLayout;
import EvesActMod.ui.layouts.PaginatedGridLayout;
import EvesActMod.ui.screens.CampaignDeckBuilderScreen;
import EvesActMod.ui.screens.CampaignScreen;
import EvesActMod.ui.buttons.IconButton;
import EvesActMod.ui.debug.DebugOverlay;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.Wiz;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.CardLibrary;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static com.badlogic.gdx.Input.Keys;

import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class CardsScreen extends CampaignScreen {
    public static final float BACK_BUTTON_SIZE = scaled(64);
    public static final float BACK_BUTTON_X = CampaignDeckBuilderScreen.LIST_X - CampaignDeckBuilderScreen.LIST_WIDTH/2f - BACK_BUTTON_SIZE/2f;
    public static final float BACK_BUTTON_Y = CampaignDeckBuilderScreen.LIST_TITLE_Y;

    public static final float CARD_GRID_MARGIN_BOTTOM = scaled(32);
    public static final float CARD_GRID_MARGIN_LEFT = scaled(128);
    public static final float CARD_GRID_WIDTH = scaled(1300);
    public static final float CARD_GRID_TOP = Settings.HEIGHT - scaled(200);
    public static final float CARD_GRID_BOTTOM = CARD_GRID_MARGIN_BOTTOM;
    public static final float CARD_GRID_HEIGHT = CARD_GRID_TOP - CARD_GRID_BOTTOM;
    public static final float CARD_GRID_X = CARD_GRID_MARGIN_LEFT + CARD_GRID_WIDTH/2f;
    public static final float CARD_GRID_Y = CARD_GRID_BOTTOM + CARD_GRID_HEIGHT/2f;
    public static final float CARD_GRID_HGAP = GridCard.WIDTH + GridCard.MARGIN * 2;
    public static final float CARD_GRID_VGAP = GridCard.HEIGHT + GridCard.MARGIN * 2;
    public static final int CARD_GRID_COLUMNS = 4;
    public static final int CARD_GRID_ROWS = 2;

    public static final float CARD_GRID_ARROW_SIZE = scaled(32);
    public static final float CARD_GRID_ARROW_SCALE = 2f;
    public static final float CARD_GRID_ARROW_RIGHT_X = CARD_GRID_X + CARD_GRID_WIDTH/2f;
    public static final float CARD_GRID_ARROW_LEFT_X = CARD_GRID_X - CARD_GRID_WIDTH/2f;
    public static final float CARD_GRID_ARROW_Y = CARD_GRID_Y;

    public static final float CARDS_LIST_GAP = InlineCard.MARGIN * 2 + InlineCard.HEIGHT;

    public static final SimpleTextField deck_title_field = new SimpleTextField(
            Wiz.centered_hitbox(CampaignDeckBuilderScreen.LIST_X, CampaignDeckBuilderScreen.LIST_TITLE_Y,
                    CampaignDeckBuilderScreen.LIST_WIDTH, scaled(32)),
            FontHelper.panelNameFont, Color.WHITE,
            Some(3), Some(20),
            (new_text) -> {
                CardCampaignGame.deck.name = new_text;
            });
    public static final IconButton back_button = new IconButton(
        AdvancedHitbox.centered(BACK_BUTTON_X, BACK_BUTTON_Y, BACK_BUTTON_SIZE, BACK_BUTTON_SIZE),
        ImageMaster.DECK_ICON, None(), None()) {
        @Override
        public void on_click() {
            deck_title_field.unfocus();
            CampaignDeckBuilderScreen.change_screen(CampaignDeckBuilderScreen.DeckBuilderMode.DECKS);
        }
    };
    public static final PaginatedGridLayout<GridCard> cards_grid = new PaginatedGridLayout<>(
            AdvancedHitbox.centered(CARD_GRID_X, CARD_GRID_Y, CARD_GRID_WIDTH, CARD_GRID_HEIGHT),
            CARD_GRID_HGAP, CARD_GRID_VGAP, CARD_GRID_COLUMNS, CARD_GRID_ROWS);
    public static final IconButton cards_grid_arrow_left = new IconButton(
            AdvancedHitbox.centered(CARD_GRID_ARROW_LEFT_X, CARD_GRID_ARROW_Y, CARD_GRID_ARROW_SIZE, CARD_GRID_ARROW_SIZE),
            IconButton.ARROW_LEFT, Some(IconButton.ARROW_LEFT_HOVER), Some(IconButton.ARROW_LEFT)) {
        @Override
        public void on_click() {
            if (!cards_grid.can_dec_page()) return;
            cards_grid.dec_page();
            reset_unpickable_overlay_scale();
            update_card_grid_arrows_scale();
        }
    };
    public static final IconButton cards_grid_arrow_right = new IconButton(
        AdvancedHitbox.centered(CARD_GRID_ARROW_RIGHT_X, CARD_GRID_ARROW_Y, CARD_GRID_ARROW_SIZE, CARD_GRID_ARROW_SIZE),
        IconButton.ARROW_RIGHT, Some(IconButton.ARROW_RIGHT_HOVER), Some(IconButton.ARROW_RIGHT)) {
        @Override
        public void on_click() {
            if (!cards_grid.can_inc_page()) return;
            cards_grid.inc_page();
            reset_unpickable_overlay_scale();
            update_card_grid_arrows_scale();
        }
    };
    public static final ListLayout<InlineCard> inlined_cards = new ListLayout<>(
        AdvancedHitbox.centered(CampaignDeckBuilderScreen.LIST_X, CampaignDeckBuilderScreen.LIST_Y,
                CampaignDeckBuilderScreen.LIST_WIDTH, CampaignDeckBuilderScreen.LIST_HEIGHT),
        ListLayout.ListMode.VERTICAL, CARDS_LIST_GAP, InlineCard.ENERGY_PADDING/2f, InlineCard.MARGIN,
        false, false);

    public static void add_card(AbstractCard card) {
        if (CardCampaignGame.deck.can_add(card)) {
            CardCampaignGame.deck.cards.add(card);
        }
        populate_inlined_cards();
    }

    public static void remove_card_safe(String card_id) {
        Option<AbstractCard> card = Option.from(
            CardCampaignGame.deck.cards.stream()
                .filter(c -> c.cardID.equals(card_id))
                .findAny());
        if (card.is_none()) return;
        CardCampaignGame.deck.cards.remove(card.unwrap_unchecked());
        populate_inlined_cards();
    }

    public static void remove_card(String card_id) {
        Option<AbstractCard> card = Option.from(
                CardCampaignGame.deck.cards.stream()
                        .filter(c -> c.cardID.equals(card_id))
                        .findAny());
        card.expect("found no match for card id '%s' in `CardsScreen::remove_card(String)`", card_id);
        CardCampaignGame.deck.cards.remove(card.unwrap_unchecked());
        populate_inlined_cards();
    }

    public static void remove_card(AbstractCard card) {
        if (!CardCampaignGame.deck.cards.contains(card)) {
            throw new UnexpectedStateError("called `CardsScreen::remove_card(AbstractCard)` with non-existent card");
        }
        CardCampaignGame.deck.cards.remove(card);
        populate_inlined_cards();
    }

    private static void populate_inlined_cards() {
        inlined_cards.set_all(CardCampaignGame.deck.cards.stream()
            .map(InlineCard::new)
            .collect(Collectors.toCollection(ArrayList::new)));
        Map<String, Integer> cards = inlined_cards.stream()
            .collect(
                HashMap::new,
                (map, card) -> map.put(card.card.cardID, map.get(card.card.cardID) == null ? 1 : map.get(card.card.cardID) + 1),
                HashMap::putAll
            );
        inlined_cards.set_all(inlined_cards.stream().filter(Wiz.distinct_by(card -> card.card.cardID)).collect(Collectors.toCollection(ArrayList::new)));
        inlined_cards.for_each(card -> card.amount = cards.get(card.card.cardID));
        inlined_cards.for_each(InlineCard::update_title);
        inlined_cards.sort((a, b) -> Wiz.compare_cards(a.card, b.card));
    }

    private static void reset_unpickable_overlay_scale()  {
        cards_grid.for_each_on_page(card -> card.unpickable_overlay_scale = GridCard.UNPICKABLE_OVERLAY_INITIAL_SCALE);
    }

    private static void update_card_grid_arrows_scale() {
        cards_grid_arrow_left.hb.scale = cards_grid.can_dec_page() ? CARD_GRID_ARROW_SCALE : 0;
        cards_grid_arrow_right.hb.scale = cards_grid.can_inc_page() ? CARD_GRID_ARROW_SCALE : 0;
    }

    public static void populate_card_grid() {
        cards_grid.clear();
        ArrayList<GridCard> grid_cards = CardLibrary.cards.values()
                .stream()
                .filter(card -> CardCampaignGame.player.getCardColor() == card.color)
                .sorted(Wiz::compare_cards)
                .map(GridCard::new)
                .collect(Collectors.toCollection(ArrayList::new));
        cards_grid.add_all(grid_cards);
    }

    @Override
    public void on_open() {
        populate_inlined_cards();
        deck_title_field.set_text(CardCampaignGame.deck.name);
        cards_grid.change_page(0);
        reset_unpickable_overlay_scale();
        update_card_grid_arrows_scale();

        DebugOverlay.register(inlined_cards);
    }

    @Override
    public void on_close() {
        inlined_cards.set_scroll(0);
        DebugOverlay.unregister(inlined_cards);
    }

    @Override
    public void update() {
        deck_title_field.update();
        back_button.update();

        if (Gdx.input.isKeyJustPressed(Keys.A) || Gdx.input.isKeyJustPressed(Keys.LEFT))
            cards_grid_arrow_left.on_click();

        if (Gdx.input.isKeyJustPressed(Keys.D) || Gdx.input.isKeyJustPressed(Keys.RIGHT))
            cards_grid_arrow_right.on_click();

        boolean shifted = Gdx.input.isKeyPressed(Keys.SHIFT_LEFT) || Gdx.input.isKeyPressed(Keys.SHIFT_RIGHT);
        for (int i = 0; i < cards_grid.elements_per_page(); i++) {
            if (Gdx.input.isKeyJustPressed(Keys.NUM_1 + i) || Gdx.input.isKeyJustPressed(Keys.NUMPAD_1 + i)) {
                cards_grid.get_on_page(i).inspect(card -> {
                    if (shifted) remove_card_safe(card.card.cardID);
                    else add_card(card.card.makeStatEquivalentCopy());
                });
            }
        }

        cards_grid_arrow_left.update();
        cards_grid_arrow_right.update();
        cards_grid.update();

        inlined_cards.update();
    }

    @Override
    public void render(SpriteBatch sb) {
        deck_title_field.render(sb);
        back_button.render(sb);

        cards_grid.render(sb);
        cards_grid_arrow_left.render(sb);
        cards_grid_arrow_right.render(sb);

        //ImageHelper.draw_centered(sb, ImageMaster.CARD_BACK, scaled(500), Settings.HEIGHT/2f, 1f);

        inlined_cards.render(sb);
    }

    static {
        cards_grid_arrow_left.hb.scale = CARD_GRID_ARROW_SCALE;
        cards_grid_arrow_right.hb.scale = CARD_GRID_ARROW_SCALE;
    }
}
