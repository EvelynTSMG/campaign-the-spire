package EvesActMod.ui.deckbuilder;

import EvesActMod.CardCampaignGame;
import EvesActMod.cards.CampaignDeck;
import EvesActMod.ui.Hittable;
import EvesActMod.ui.screens.CampaignDeckBuilderScreen;
import EvesActMod.ui.Renderable;
import EvesActMod.util.ImageHelper;
import EvesActMod.util.TextHelper;
import EvesActMod.util.Timer;
import EvesActMod.util.Wiz;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static EvesActMod.common.Option.None;
import static EvesActMod.util.ImageHelper.scaled;

public class InlineDeck extends Hittable {
    public static final BitmapFont TITLE_FONT = TextHelper.prep_font(TextHelper.get_default_font(), 32f, false, 1f,  0, 0, Color.BLACK, false, 2f, 1f, Color.BLACK.cpy().mul(0), 0, 0);

    public static final float WIDTH = scaled(348);
    public static final float HEIGHT = scaled(56);

    public static final float PADDING = scaled(4);
    public static final float MARGIN = scaled(8);

    public static final float PRETTINESS_LENGTH = 0.2f;

    public static final TextureRegion BORDER = new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/inline_deck_border.png"));

    public static final TextureRegion BACKGROUND = new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/inline_deck_bg.png"));
    public static final int MAX_BACKGROUND_SEGMENTS = 4;
    public static final float BACKGROUND_LEAN_DEGREES = 15;
    public static final float BACKGROUND_OFFSET = (float)(Math.tan(Math.toRadians(BACKGROUND_LEAN_DEGREES)) * HEIGHT/2f);
    private static final FrameBuffer BACKGROUND_BUFFER = ImageHelper.create_buffer(WIDTH, HEIGHT);
    private static final OrthographicCamera BACKGROUND_CAMERA = new OrthographicCamera(WIDTH, HEIGHT);

    private static final TextureRegion DEBUG_MARKER = new TextureRegion(ImageMaster.CHECKBOX);

    public Hitbox hb = new Hitbox(WIDTH, HEIGHT);
    public Hitbox hover_hb = new Hitbox(WIDTH, HEIGHT + MARGIN * 2);
    public CampaignDeck deck;
    protected TextureRegion background;

    private final Timer prettiness_timer = new Timer(PRETTINESS_LENGTH, false, false);
    public boolean pretty = false;

    public InlineDeck(CampaignDeck deck) {
        this.deck = deck;
        background = create_background(deck);
    }

    public void set_pos(float x, float y) {
        hb.move(x, y);
        hover_hb.move(x, y - MARGIN);
    }

    public TextureRegion create_background(CampaignDeck deck) {
        ArrayList<AbstractCard> cards = deck.cards.stream()
                .sorted(Wiz::compare_cards)
                .collect(Collectors.toCollection(ArrayList::new));
        int segments = Math.min(cards.size(), MAX_BACKGROUND_SEGMENTS);

        if (segments == 0) return BACKGROUND;
        if (segments == 1) return new TextureRegion(ImageHelper.make_rectangle_portrait(cards.get(0), None()));

        Texture[] arts = new Texture[segments];
        for (int i = 0; i < segments; i++) {
            arts[i] = ImageHelper.make_rectangle_portrait(cards.get(i), None());
        }

        float segment_width = WIDTH/segments;
        float[] tl, tr, bl, br;
        tl = new float[segments + 1];
        tr = new float[segments + 1];
        bl = new float[segments + 1];
        br = new float[segments + 1];

        for (int i = segments; i >= 0; i--) {
            tl[i] = i * segment_width + BACKGROUND_OFFSET/2f;
            tr[i] = tl[i] + segment_width;
            bl[i] = tl[i] - BACKGROUND_OFFSET;
            br[i] = bl[i] + segment_width;
        }

        tl[0] = 0;
        bl[0] = 0;
        tr[segments] = WIDTH;
        br[segments] = WIDTH;SpriteBatch sb = new SpriteBatch();
        sb.setProjectionMatrix(BACKGROUND_CAMERA.combined);

        BACKGROUND_BUFFER.begin();
        ImageHelper.swap_texture_and_clear(BACKGROUND_BUFFER);
        sb.begin();

        ImageHelper.draw_centered(sb, BACKGROUND, hb.cX, hb.cY, 1f);
        for (int i = segments; i >= 0; i--) {
            ImageHelper.draw_centered(sb, DEBUG_MARKER, tl[i], HEIGHT, 1f);
            ImageHelper.draw_centered(sb, DEBUG_MARKER, tr[i], HEIGHT, 1f);
            ImageHelper.draw_centered(sb, DEBUG_MARKER, bl[i], HEIGHT, 1f);
            ImageHelper.draw_centered(sb, DEBUG_MARKER, br[i], HEIGHT, 1f);
        }

        sb.end();
        BACKGROUND_BUFFER.end();

        return ImageHelper.buffer_texture(BACKGROUND_BUFFER);
    }

    @Override
    public void update() {
        hb.update();
        hover_hb.update();

        if ((hover_hb.hovered ^ pretty) && prettiness_timer.is_stopped()) {
            pretty = !pretty;
            prettiness_timer.start();
        }

        if (hover_hb.hovered && InputHelper.justReleasedClickLeft) {
            CardCampaignGame.deck = deck;
            CampaignDeckBuilderScreen.change_screen(CampaignDeckBuilderScreen.DeckBuilderMode.CARDS);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        prettiness_timer.tick();

        sb.setColor(pretty ? Color.CYAN : Color.WHITE);

        float prettiness_percentage = 1 - prettiness_timer.get_percentage();
        float width = WIDTH * prettiness_percentage;
        ImageHelper.draw_centered(sb, BORDER, hb.cX, hb.cY, 1f);
        ImageHelper.scissors(sb, hb.x + width, hb.y, WIDTH - width, HEIGHT,
                () -> {
                    sb.setColor(pretty ? Color.WHITE : Color.CYAN);
                    ImageHelper.draw_centered(sb, BORDER, hb.cX, hb.cY, 1f);
                });
        sb.setColor(Color.WHITE);
        ImageHelper.draw_centered(sb, BACKGROUND, hb.cX, hb.cY, 1f);

        FontHelper.renderFontCentered(sb, TITLE_FONT, deck.name, hb.cX, hb.cY, Color.WHITE);

        hb.render(sb);
    }
}
