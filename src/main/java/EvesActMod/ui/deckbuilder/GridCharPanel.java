package EvesActMod.ui.deckbuilder;

import EvesActMod.CardCampaignGame;
import EvesActMod.books.AudioBook;
import EvesActMod.common.Option;
import EvesActMod.ui.Hittable;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.AudioHelper;
import EvesActMod.util.ImageHelper;
import EvesActMod.books.ShaderBook;
import EvesActMod.util.TextHelper;
import EvesActMod.util.Wiz;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import java.util.HashMap;
import java.util.Map;

import static EvesActMod.util.ImageHelper.scaled;

public class GridCharPanel extends Hittable {
    public static final float WIDTH = DecksScreen.CHAR_GRID_WIDTH/DecksScreen.CHAR_GRID_COLUMNS;
    public static final float HEIGHT = DecksScreen.CHAR_GRID_HEIGHT/DecksScreen.CHAR_GRID_ROWS;

    public static final int BG_OFFSET_X = 1230;
    public static final int BG_OFFSET_Y = 176;
    public static final int BG_WIDTH = 650;
    public static final int BG_HEIGHT = 848;
    public static final float BG_SCALE = Math.max(WIDTH/scaled(BG_WIDTH), HEIGHT/scaled(BG_HEIGHT));

    public static final BitmapFont NAME_FONT = TextHelper.prep_font(TextHelper.get_default_font(), 30f, false, 1f,  0, 0, Color.BLACK.cpy().sub(0, 0, 0, 0.2f), false, 1.5f, 1f, Color.BLACK.cpy().mul(0), 0, 0);
    public static final BitmapFont SMOL_NAME_FONT = TextHelper.prep_font(TextHelper.get_default_font(), 20f, false, 1f,  0, 0, Color.BLACK.cpy().sub(0, 0, 0, 0.2f), false, 1.5f, 1f, Color.BLACK.cpy().mul(0), 0, 0);

    public static final TextureRegion NAME_BACKGROUND =
        new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/grid_character_name_bg.png"));
    public static final float NAME_Y = HEIGHT * 0.15f;

    public static final Color NAME_COLOR = Color.WHITE;

    public static final ShaderProgram DEFAULT_SHADER = ShaderBook.VIGNETTE_WHITE;
    public static final float HOVER_EFFECT_TRANS_LENGTH = 0.5f;

    protected static final Map<AbstractPlayer.PlayerClass, ShaderProgram> shaders = new HashMap<>();
    protected static final Map<AbstractPlayer.PlayerClass, Color> shader_colors = new HashMap<>();
    protected static final Map<AbstractPlayer.PlayerClass, TextureRegion> backgrounds = new HashMap<>();

    public final AbstractPlayer.PlayerClass cls;

    public boolean selected;
    protected final String char_name;
    protected final Color char_color;
    protected final Color selected_name_color;
    protected float hover_time;
    protected final boolean uses_smol_font;
    protected final boolean invert_name;
    protected float hover_trans_progress;

    static {
        for (AbstractPlayer character : CardCampaignGame.characters) {
            Option<Texture> background = ImageHelper.get_character_portrait(character.chosenClass);
            background.inspect(bg -> backgrounds.put(character.chosenClass,
                    new TextureRegion(bg, BG_OFFSET_X, BG_OFFSET_Y, BG_WIDTH, BG_HEIGHT)));
        }

        // Watcher's in the center of her background, rendering the normal approach insufficient
        set_background(AbstractPlayer.PlayerClass.WATCHER, ImageMaster.CHAR_SELECT_BG_WATCHER, 770, 123);

        // Set up basegame shaders, since basegame won't magically change to include Campaign support
        set_shader(AbstractPlayer.PlayerClass.IRONCLAD, ShaderBook.FLYERS_FIRE);
        set_shader(AbstractPlayer.PlayerClass.THE_SILENT, ShaderBook.FOG);

        set_shader(AbstractPlayer.PlayerClass.WATCHER, ShaderBook.LIGHT_BEAMS);

        // Set up basegame shader colors for ✨style✨
        set_shader_color(AbstractPlayer.PlayerClass.IRONCLAD, new Color(0xff660dff));
        set_shader_color(AbstractPlayer.PlayerClass.THE_SILENT, Wiz.get_character_color(AbstractPlayer.PlayerClass.THE_SILENT).mul(0.5f));
    }

    public GridCharPanel(AbstractPlayer.PlayerClass cls) {
        this.hb = new AdvancedHitbox(WIDTH, HEIGHT);
        this.cls = cls;
        char_name = Wiz.get_character_name(cls);
        char_color = Wiz.get_character_color(cls);
        uses_smol_font = TextHelper.get_text_width(NAME_FONT, Wiz.get_character_name(cls)) > WIDTH;
        float min_rgb = Wiz.min(char_color.r, char_color.g, char_color.b);
        float max_rgb = Wiz.max(char_color.r, char_color.g, char_color.b);
        invert_name = (min_rgb + max_rgb) / 2 < 0.5;
        selected_name_color = invert_name ? Wiz.invert(char_color) : char_color;
    }

    public static void set_background(AbstractPlayer.PlayerClass cls, Texture image, int x, int y) {
        backgrounds.put(cls, new TextureRegion(image, x, y, BG_WIDTH, BG_HEIGHT));
    }

    public static void set_shader(AbstractPlayer.PlayerClass cls, ShaderProgram shader) {
        shaders.put(cls, shader);
    }

    public static void set_shader_color(AbstractPlayer.PlayerClass cls, Color color) {
        shader_colors.put(cls, color);
    }

    @Override
    public void update() {
        hb.update();

        if (hover_trans_progress > 0) hover_time += Gdx.graphics.getDeltaTime();
        else hover_time = 0;

        if (hb.hovered) hover_trans_progress += Gdx.graphics.getDeltaTime() / HOVER_EFFECT_TRANS_LENGTH;
        else hover_trans_progress -= Gdx.graphics.getDeltaTime() / HOVER_EFFECT_TRANS_LENGTH;
        hover_trans_progress = MathUtils.clamp(hover_trans_progress, 0, 1f);

        if (hb.hovered && InputHelper.justClickedLeft) {
            DecksScreen.select_character(cls);
        }

        if (hb.justHovered) {
            AudioHelper.play(AudioBook.UI_HOVER, 1f);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        ImageHelper.scissors(sb, hb, () -> {
            if (hover_trans_progress > 0) {
                ShaderProgram shader = shaders.getOrDefault(cls, DEFAULT_SHADER);
                sb.setShader(shader);
                if (shader.hasUniform("u_imagePos"))
                    shader.setUniform2fv("u_imagePos", new float[] { hb.cX(), hb.cY() }, 0 ,2);
                if (shader.hasUniform("u_imageSize"))
                    shader.setUniform2fv("u_imageSize", new float[] { hb.width(), hb.height() }, 0 ,2);
                if (shader.hasUniform("u_charColor"))
                    shader.setUniform3fv("u_charColor", Wiz.vec3_from(shader_colors.getOrDefault(cls, char_color)), 0, 3);
                if (shader.hasUniform("u_time")) shader.setUniformf("u_time", hover_time);
                if (shader.hasUniform("u_alpha")) shader.setUniformf("u_alpha", hover_trans_progress);
            }

            if (backgrounds.containsKey(cls))
                ImageHelper.draw_centered(sb, backgrounds.get(cls), hb.cX(), hb.cY(), BG_SCALE);

            sb.setShader(null);
            ImageHelper.draw_centered(sb, NAME_BACKGROUND, hb.cX(), hb.bottom() + NAME_Y, BG_SCALE);

            if (invert_name) sb.setShader(ShaderBook.INVERT);
            FontHelper.renderFontCentered(sb, uses_smol_font ? SMOL_NAME_FONT : NAME_FONT, char_name,
                    hb.cX(), hb.bottom() + NAME_Y, selected ? selected_name_color : NAME_COLOR);

            sb.setShader(null);
        });
        hb.render(sb);
    }
}
