package EvesActMod.ui.deckbuilder;

import EvesActMod.CardCampaignGame;
import EvesActMod.ui.Hittable;
import EvesActMod.util.ImageHelper;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.books.ShaderBook;
import EvesActMod.util.Wiz;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.megacrit.cardcrawl.cards.AbstractCard;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import static EvesActMod.util.ImageHelper.scaled;

public class GridCard extends Hittable {
    public static final float SCALE = 0.8f;
    public static final float HOVERED_SCALE = 0.9f;
    public static final float SCALE_SPEED = 6f;

    public static final float WIDTH = AbstractCard.IMG_WIDTH * SCALE;
    public static final float HEIGHT = AbstractCard.IMG_HEIGHT * SCALE;
    public static final float MARGIN = scaled(32);
    public static final float BUMP_HEIGHT = scaled(8);
    public static final float BUMP_SPEED = 4f;

    public static final TextureRegion UNPICKABLE_OVERLAY = new TextureRegion(ImageHelper.texture_at("ui/deckbuilder/unpickable_card_overlay.png"));
    public static final float UNPICKABLE_OVERLAY_INITIAL_SCALE = 0.5f;
    public static final float UNPICKABLE_OVERLAY_SCALE = 0.9f;
    public static final float UNPICKABLE_OVERLAY_HOVERED_SCALE = 1.4f;
    public static final float UNPICKABLE_OVERLAY_SCALE_UP_SPEED = 32f;
    public static final float UNPICKABLE_OVERLAY_SCALE_DOWN_SPEED = 5f;

    public AbstractCard card;
    protected AbstractCard copy_card;
    protected float unpickable_overlay_scale = UNPICKABLE_OVERLAY_INITIAL_SCALE;

    public GridCard(AbstractCard card) {
        hb = new AdvancedHitbox(WIDTH, HEIGHT);
        this.card = card;
        copy_card = card.makeStatEquivalentCopy();
        copy_card.drawScale = SCALE;
    }

    protected boolean can_pick() {
        return CardCampaignGame.deck.can_add(card);
    }

    @Override
    public void update() {
        hb.update();

        boolean can_pick = can_pick();
        copy_card.targetDrawScale = hb.hovered && can_pick ? HOVERED_SCALE : SCALE;
        copy_card.drawScale = Wiz.lerp(copy_card.drawScale, copy_card.targetDrawScale, SCALE_SPEED, Settings.CARD_SCALE_SNAP_THRESHOLD);
        hb.scale = copy_card.drawScale + (HOVERED_SCALE - SCALE);

        unpickable_overlay_scale = Wiz.lerp(unpickable_overlay_scale,
                hb.hovered && can_pick ? UNPICKABLE_OVERLAY_HOVERED_SCALE : UNPICKABLE_OVERLAY_SCALE,
                hb.hovered && can_pick ? UNPICKABLE_OVERLAY_SCALE_UP_SPEED : UNPICKABLE_OVERLAY_SCALE_DOWN_SPEED,
                Settings.CARD_SCALE_SNAP_THRESHOLD);

        hb.offset_y = Wiz.lerp(hb.offset_y, hb.hovered && can_pick ? BUMP_HEIGHT : 0, BUMP_SPEED, ImageHelper.PIXEL_SNAP);

        if (hb.hovered && can_pick && InputHelper.justClickedLeft) {
            CardsScreen.add_card(card.makeStatEquivalentCopy());
        }

        if (hb.hovered && InputHelper.justClickedRight) {
            CardsScreen.remove_card_safe(card.cardID);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        boolean unpickable = !can_pick();
        if (unpickable) sb.setShader(ShaderBook.GRAYSCALE);

        copy_card.current_x = hb.cX();
        copy_card.current_y = hb.cY();
        copy_card.render(sb);

        sb.setShader(null);
        if (unpickable) {
            ImageHelper.draw_centered(sb, UNPICKABLE_OVERLAY, hb.cX(), hb.cY(), unpickable_overlay_scale);
        }
        hb.render(sb);
    }
}
