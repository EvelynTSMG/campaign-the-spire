package EvesActMod.ui.input;

import EvesActMod.common.Option;
import EvesActMod.ui.Renderable;
import EvesActMod.util.Timer;
import basemod.interfaces.TextReceiver;
import basemod.patches.com.megacrit.cardcrawl.helpers.input.ScrollInputProcessor.TextInput;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import java.util.function.Consumer;

import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;
import static EvesActMod.util.ImageHelper.scaled;

public class SimpleTextField implements Renderable, TextReceiver {
    public static final float CARET_INTERVAL = 0.5f;
    public final Hitbox hb;
    public BitmapFont font;
    public Color color;
    public int min_length = 0;
    public int max_length = Integer.MAX_VALUE;
    public Consumer<String> on_edited;
    private String text = "";
    private String old_text;
    private boolean is_editing;
    private boolean just_stopped_editing;
    private final Timer caret_timer = new Timer(CARET_INTERVAL, true, true);
    private boolean caret = true;

    public SimpleTextField(Hitbox hitbox, BitmapFont font, Color color, Option<Integer> min_length, Option<Integer> max_length, Consumer<String> on_edited) {
        hb = hitbox;
        this.font = font;
        this.color = color;
        min_length.inspect(min -> this.min_length = min);
        max_length.inspect(max -> this.max_length = max);
        this.on_edited = on_edited;
    }

    public Option<Integer> set_text(String text) {
        int length = text.length();
        if (min_length <= length && length <= max_length) {
            this.text = text;
            return None();
        }

        if (min_length > length) return Some(min_length - length);
        // if (max_length < length)
        return Some(max_length - length);
    }

    public void unfocus() {
        is_editing = false;
        just_stopped_editing = false;
        TextInput.stopTextReceiver(this);
        if (min_length <= text.length() && text.length() <= max_length) {
            on_edited.accept(text);
        } else {
            text = old_text;
        }
    }

    @Override
    public void update() {
        hb.update();

        if (!is_editing) {
            is_editing = hb.hovered && InputHelper.justReleasedClickRight;
            if (is_editing) { // started editing
                TextInput.startTextReceiver(this);
                old_text = text;
            }
        } else {
            is_editing = hb.hovered || !(InputHelper.justReleasedClickLeft || InputHelper.justReleasedClickRight);
            just_stopped_editing = !is_editing;
            if (just_stopped_editing) unfocus();
        }

        if (just_stopped_editing) {
            just_stopped_editing = false;
        }

        if (is_editing) {
            caret_timer.paused = false;
        } else {
            caret_timer.paused = true;
            caret_timer.start();
            caret = true;
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        if (caret_timer.tick() == 0) caret = !caret;

        FontHelper.renderFontCentered(sb, font, is_editing && caret ? text + '|' : text, hb.cX, hb.cY, color);
        hb.render(sb);
    }

    @Override
    public String getCurrentText() {
        return text;
    }

    @Override
    public void setText(String s) {
        text = s;
    }

    @Override
    public boolean isDone() {
        return !is_editing;
    }

    @Override
    public boolean acceptCharacter(char c) {
        return text.length() < max_length;
    }

    @Override
    public boolean onPushBackspace() {
        if (Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) || Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT)) {
            int last_space = text.lastIndexOf(' ');
            if (last_space == -1) text = "";
            else text = text.substring(0, last_space);
            return true;
        }
        return false;
    }

    @Override
    public boolean onPushEnter() {
        just_stopped_editing = is_editing;
        is_editing = false;
        return true;
    }
}
