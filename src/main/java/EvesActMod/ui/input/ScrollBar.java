package EvesActMod.ui.input;

import EvesActMod.common.errors.UnreachableReachedError;
import EvesActMod.ui.Hittable;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.core.Settings;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.ImageMaster;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static EvesActMod.util.ImageHelper.scaled;

public class ScrollBar extends Hittable {
    public static final float WIDTH = scaled(54);
    public static final float PADDING = scaled(8);
    public static final float TRAIN_OFFSET = ScrollBarTrain.LONG/2f + PADDING;

    public enum ScrollBarDirection {
        VERTICAL(ImageMaster.SCROLL_BAR_TRAIN,
                 ImageMaster.SCROLL_BAR_BOTTOM,
                 ImageMaster.SCROLL_BAR_TOP,
                 ImageMaster.SCROLL_BAR_MIDDLE),
        HORIZONTAL(ImageMaster.SCROLL_BAR_HORIZONTAL_TRAIN,
                   ImageMaster.SCROLL_BAR_LEFT,
                   ImageMaster.SCROLL_BAR_RIGHT,
                   ImageMaster.SCROLL_BAR_HORIZONTAL_MIDDLE);

        public final TextureRegion train;
        public final TextureRegion blend;
        public final TextureRegion trend;
        public final TextureRegion mid;

        ScrollBarDirection(Texture train, Texture blend, Texture trend, Texture mid) {
            this.train = new TextureRegion(train);
            this.blend = new TextureRegion(blend);
            this.trend = new TextureRegion(trend);
            this.mid = new TextureRegion(mid);
        }
    }

    public final ScrollBarDirection direction;
    public float scroll_speed = 1f;
    protected final Consumer<Float> on_scroll;
    protected final ScrollBarTrain train;
    protected boolean reverse;
    protected boolean enabled = true;
    protected float scroll;

    public ScrollBar(AdvancedHitbox hitbox, ScrollBarDirection direction, boolean reverse, Consumer<Float> on_scroll) {
        this.hb = hitbox;
        this.direction = direction;
        this.reverse = reverse;
        this.on_scroll = on_scroll;

        switch (direction) {
            case VERTICAL:
                train = new ScrollBarTrain(direction, hb.cX(), reverse ? hb.bottom() + TRAIN_OFFSET : hb.top() - ScrollBarTrain.LONG/2f - PADDING, hb.scale);
                break;
            case HORIZONTAL:
                train = new ScrollBarTrain(direction, reverse ? hb.right() - ScrollBarTrain.LONG/2f - PADDING : hb.left() + TRAIN_OFFSET, hb.cY(), hb.scale);
                break;
            default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
        }
    }

    protected void clamp_train_pos() {
        switch (direction) {
            case VERTICAL:
                train.hb.clamp_pos(hb.cX(), hb.cX(), hb.bottom() + TRAIN_OFFSET, hb.top() - ScrollBarTrain.LONG/2f - PADDING);
                break;
            case HORIZONTAL:
                train.hb.clamp_pos(hb.left() + TRAIN_OFFSET, hb.right() - ScrollBarTrain.LONG/2f - PADDING, hb.cY(), hb.cY());
                break;
            default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
        }
    }

    protected void update_train_pos() {
        float total_length, pos;
        switch (direction) {
            case VERTICAL:
                total_length = hb.height() - TRAIN_OFFSET*2;
                pos = scroll * total_length;
                pos += reverse ? -TRAIN_OFFSET : TRAIN_OFFSET;
                train.hb.move(hb.cX(), reverse ? (hb.bottom() + pos) : (hb.top() - pos));
                break;
            case HORIZONTAL:
                total_length = hb.width() - TRAIN_OFFSET*2;
                pos = scroll * total_length;
                pos += reverse ? TRAIN_OFFSET : -TRAIN_OFFSET;
                train.hb.move(reverse ? (hb.right() - pos) : (hb.left() + pos), hb.cY());
                break;
        }
    }

    public void set_enabled(boolean enabled) {
        this.enabled = enabled;
        if (enabled) return;
        set_scroll(0);
    }

    public void set_scroll(float value) {
        scroll = MathUtils.clamp(value, 0, 1);
        on_scroll.accept(scroll);
        update_train_pos();
    }

    @Override
    public void update() {
        if (!enabled) return;
        hb.update();

        train.update();
        if (train.held) {
            clamp_train_pos();
            float pos, total_length;

            switch (direction) {
                case VERTICAL:
                    pos = reverse
                            ? (train.hb.cY() - hb.bottom())
                            : (hb.top() - train.hb.cY());
                    pos -= TRAIN_OFFSET;
                    total_length = hb.height() - TRAIN_OFFSET*2;
                    set_scroll(pos / total_length);
                    break;
                case HORIZONTAL:
                    pos = reverse
                            ? (hb.right() - train.hb.cX())
                            : (train.hb.cX() - hb.left());
                    pos -= TRAIN_OFFSET;
                    total_length = hb.width() - TRAIN_OFFSET*2;
                    set_scroll(pos / total_length);
                    break;
                default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
            }
        } else if (hb.hovered) {
            if  (InputHelper.scrolledUp)  set_scroll(scroll - scroll_speed);
            if (InputHelper.scrolledDown) set_scroll(scroll + scroll_speed);

            if (Gdx.input.isKeyJustPressed(Input.Keys.HOME)) set_scroll(0);
            if (Gdx.input.isKeyJustPressed(Input.Keys.END)) set_scroll(1f);
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        hb.render(sb);
        switch (direction) {
            case VERTICAL:
                ImageHelper.draw_centered(sb, direction.trend, hb.cX(), hb.top(), hb.scale);
                ImageHelper.draw_centered(sb, direction.blend, hb.cX(), hb.bottom(), hb.scale);

                ImageHelper.scissors(sb, hb.left(), hb.bottom() + WIDTH/2f, hb.width(), hb.height(), () -> {
                    float gap = reverse ? WIDTH : -WIDTH;
                    float pos = (reverse ? hb.bottom() : hb.top()) + gap;
                    float limit = (reverse ? hb.top() : hb.bottom()) - gap/2f;
                    Predicate<Float> test = reverse ? y -> y < limit : y -> y > limit;

                    for (;test.test(pos); pos += gap) {
                        ImageHelper.draw_centered(sb, direction.mid, hb.cX(), pos, hb.scale);
                    }
                });
                break;
            case HORIZONTAL:
                ImageHelper.draw_centered(sb, direction.blend, hb.left(), hb.cY(), hb.scale);
                ImageHelper.draw_centered(sb, direction.trend, hb.right(), hb.cY(), hb.scale);

                ImageHelper.scissors(sb, hb.left() + WIDTH/2f, hb.bottom(), hb.width(), hb.height(), () -> {
                    float gap = reverse ? WIDTH : -WIDTH;
                    float pos = (reverse ? hb.right() : hb.left()) + gap;
                    float limit = (reverse ? hb.left() : hb.right()) - gap/2f;
                    Predicate<Float> test = reverse ? x -> x < limit : x -> x > limit;

                    for (;test.test(pos); pos += gap) {
                        ImageHelper.draw_centered(sb, direction.mid, pos, hb.cY(), hb.scale);
                    }
                });
                break;
            default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
        }

        if (!enabled) return;
        train.render(sb);
        if (Settings.isDebug) {
            FontHelper.renderFontCentered(sb, FontHelper.powerAmountFont, String.format("%.0f%%", scroll * 100),
                    train.hb.cX(), train.hb.cY(), Color.WHITE);
        }
    }
}
