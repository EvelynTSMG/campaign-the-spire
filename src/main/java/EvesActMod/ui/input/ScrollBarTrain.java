package EvesActMod.ui.input;

import EvesActMod.common.errors.UnreachableReachedError;
import EvesActMod.ui.Hittable;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.megacrit.cardcrawl.helpers.FontHelper;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

import static EvesActMod.util.ImageHelper.scaled;

public class ScrollBarTrain extends Hittable {
    public static final float LONG = scaled(60);
    public static final float SHORT = scaled(38);

    public boolean held;
    public ScrollBar.ScrollBarDirection direction;
    protected TextureRegion image;

    public ScrollBarTrain(ScrollBar.ScrollBarDirection direction, float x, float y, float scale) {
        this.image = direction.train;
        this.direction = direction;

        hb = AdvancedHitbox.centered(x, y, image.getRegionWidth(), image.getRegionHeight());
        hb.scale = scale;
        hb.move(x, y);
    }

    @Override
    public void update() {
        hb.update();
        if (held || hb.hovered) held = InputHelper.isMouseDown;
        if (held) {
            switch (direction) {
                case VERTICAL: hb.moveY(InputHelper.mY); break;
                case HORIZONTAL: hb.moveX(InputHelper.mX); break;
                default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
            }
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        ImageHelper.draw_centered(sb, image, hb.cX(), hb.cY(), hb.scale);
        hb.render(sb);
    }
}
