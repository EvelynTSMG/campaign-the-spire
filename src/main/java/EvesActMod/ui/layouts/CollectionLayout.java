package EvesActMod.ui.layouts;

import EvesActMod.ui.Hittable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.function.Consumer;
import java.util.stream.Stream;

public abstract class CollectionLayout<T extends Hittable> extends Hittable {
    protected ArrayList<T> elements = new ArrayList<>();

    public T get(int index) {
        return elements.get(index);
    }

    public void set(int index, T value) {
        elements.set(index, value);
    }

    public void set_all(ArrayList<T> elements) {
        this.elements = elements;
    }

    public void add_all(Collection<? extends T> elements) {
        this.elements.addAll(elements);
        on_elements_modified();
    }

    public void add(T element) {
        elements.add(element);
        on_elements_modified();
    }

    public void remove(T element) {
        elements.remove(element);
        on_elements_modified();
    }

    public void clear() {
        elements.clear();
        on_elements_modified();
    }

    public Stream<T> stream() {
        return elements.stream();
    }

    public void for_each(Consumer<T> action) {
        elements.forEach(action);
    }

    public void sort(Comparator<T> comparator) {
        elements.sort(comparator);
    }

    protected void on_elements_modified() { }
}
