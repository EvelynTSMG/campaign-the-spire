package EvesActMod.ui.layouts;

import EvesActMod.common.errors.UnreachableReachedError;
import EvesActMod.ui.Hittable;
import EvesActMod.ui.input.ScrollBar;
import EvesActMod.ui.debug.AddsDebugInfo;
import EvesActMod.ui.debug.DebugInfoBlock;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

public class ListLayout<T extends Hittable> extends CollectionLayout<T> implements AddsDebugInfo {
    public enum ListMode {
        VERTICAL(ScrollBar.ScrollBarDirection.VERTICAL),
        HORIZONTAL(ScrollBar.ScrollBarDirection.HORIZONTAL);

        public final ScrollBar.ScrollBarDirection scroll_bar_direction;

        ListMode(ScrollBar.ScrollBarDirection scroll_bar_direction) {
            this.scroll_bar_direction = scroll_bar_direction;
        }
    }

    public static final float SCROLL_SPEED = 7.5f;

    public ListMode mode;
    public float offset;
    public float padding;
    protected float scroll;
    protected float gap;
    protected boolean reverse;
    public boolean flip_scroll_bar;
    protected ScrollBar scroll_bar;

    public ListLayout(AdvancedHitbox hitbox, ListMode mode,
                      float gap, float offset, float padding,
                      boolean reverse, boolean flip_scroll_bar) {
        this.hb = hitbox;
        this.mode = mode;
        this.gap = gap;
        this.offset = offset;
        this.padding = padding;
        this.reverse = reverse;
        this.flip_scroll_bar = flip_scroll_bar;

        AdvancedHitbox scroll_bar_hb;
        switch (mode) {
            case VERTICAL: scroll_bar_hb = AdvancedHitbox.centered(flip_scroll_bar ? hb.left() - ScrollBar.WIDTH/2f : hb.right() + ScrollBar.WIDTH/2f, hb.cY(), ScrollBar.WIDTH, hb.height); break;
            case HORIZONTAL: scroll_bar_hb = AdvancedHitbox.centered(hb.cX(), flip_scroll_bar ? hb.top() + ScrollBar.WIDTH/2f : hb.bottom() - ScrollBar.WIDTH/2f, hb.width, ScrollBar.WIDTH); break;
            default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
        }

        scroll_bar = new ScrollBar(scroll_bar_hb, mode.scroll_bar_direction, reverse, this::set_scroll_internal);
    }

    public boolean needs_scrolling() {
        return gap * elements.size() > (mode == ListMode.VERTICAL ? hb.height() : hb.width());
    }

    private int elements_until_scrollable() {
        float target = mode == ListMode.VERTICAL ? hb.height() : hb.width();
        return (int)Math.ceil(target / gap);
    }

    protected float scroll_mult() {
        float mult = SCROLL_SPEED/elements.size();
        return reverse ? -mult : mult;
    }

    protected float scroll_offset() {
        return Math.max(0, scroll * (elements.size() - (elements_until_scrollable() - 1)) * gap);
    }

    protected float max_scroll_offset() {
        return Math.max(0, (elements.size() - (elements_until_scrollable() - 1)) * gap);
    }

    protected void set_scroll_internal(float value) {
        scroll = MathUtils.clamp(value, 0, 1);
    }

    public void set_scroll(float value) {
        scroll_bar.set_scroll(value);
    }

    @Override
    protected void on_elements_modified() {
        scroll_bar.scroll_speed = scroll_mult();
    }

    @Override
    public void update() {
        hb.update();
        if (elements.isEmpty()) return;

        boolean needs_scrolling = needs_scrolling();
        if (hb.hovered && needs_scrolling) {
            if  (InputHelper.scrolledUp)  set_scroll(scroll - scroll_mult());
            if (InputHelper.scrolledDown) set_scroll(scroll + scroll_mult());

            // Page Up and Page Down don't work properly but I don't know what I'm doing
            float page_size = (gap * (elements_until_scrollable() - 1));
            float scroll_size = page_size / (gap * elements.size());
            if  (Gdx.input.isKeyJustPressed(Input.Keys.PAGE_UP))  set_scroll(scroll - scroll_size);
            if (Gdx.input.isKeyJustPressed(Input.Keys.PAGE_DOWN)) set_scroll(scroll + scroll_size);

            if (Gdx.input.isKeyJustPressed(Input.Keys.HOME)) set_scroll(0);
            if (Gdx.input.isKeyJustPressed(Input.Keys.END))  set_scroll(1f);
        }

        scroll_bar.set_enabled(needs_scrolling);
        scroll_bar.update();

        float gap, half_size, pos;
        switch (mode) {
            case VERTICAL:
                scroll_bar.hb.move(flip_scroll_bar
                        ? hb.left() - ScrollBar.WIDTH/2f
                        : hb.right() + ScrollBar.WIDTH/2f, hb.cY());
                gap = reverse ? this.gap : -this.gap;
                half_size = elements.get(0).hb.height()/2f;
                pos = reverse ? hb.bottom() + half_size : hb.top() - half_size;
                pos += reverse ? -scroll_offset() : scroll_offset();
                for (T e : elements) {
                    e.hb.move(offset + hb.cX(), (reverse ? padding : -padding) + pos);
                    e.update();
                    pos += gap;
                }
                break;
            case HORIZONTAL:
                scroll_bar.hb.move(hb.cX(), flip_scroll_bar
                        ? hb.top() + ScrollBar.WIDTH/2f
                        : hb.bottom() - ScrollBar.WIDTH/2f);
                gap = reverse ? -this.gap : this.gap;
                half_size = elements.get(0).hb.width()/2f;
                pos = reverse ? hb.right() - half_size : hb.left() + half_size;
                pos += reverse ? scroll_offset() : -scroll_offset();
                for (T e : elements) {
                    e.hb.move((reverse ? padding : -padding) + pos, offset + hb.cY());
                    e.update();
                    pos += gap;
                }
                break;
            default: throw new UnreachableReachedError("switch statement is no longer exhaustive");
        }
    }

    @Override
    public void render(SpriteBatch sb) {
        hb.render(sb);

        ImageHelper.scissors(sb, hb, () -> {
            for (T element : elements) {
                if (element.hb.intersects(hb)) {
                    element.render(sb);
                }
            }
        });

        scroll_bar.render(sb);
    }

    @Override
    public DebugInfoBlock add_block() {
        if (needs_scrolling()) {
            return new DebugInfoBlock(
                DebugInfoBlock.header_from(this),
                String.format("Elements: %d (scrollable)", elements.size()),
                String.format("Scroll: %.0f%% (%.2f out of %.2f offset)",
                        scroll * 100,
                        (mode == ListMode.VERTICAL ^ reverse) ? -scroll_offset() : scroll_offset(),
                        (mode == ListMode.VERTICAL ^ reverse) ? -max_scroll_offset() : max_scroll_offset())
            );
        } else {
            return new DebugInfoBlock(
                DebugInfoBlock.header_from(this),
                String.format("Elements: %d (%d until scrollable)", elements.size(), elements_until_scrollable() - elements.size()),
                "Scroll: None"
            );
        }
    }
}
