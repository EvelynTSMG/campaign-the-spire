package EvesActMod.ui.layouts;

import EvesActMod.common.Option;
import EvesActMod.ui.Hittable;
import EvesActMod.ui.Renderable;
import EvesActMod.util.AdvancedHitbox;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Arrays;
import java.util.function.Consumer;

import static EvesActMod.common.Option.None;
import static EvesActMod.common.Option.Some;

public class PaginatedGridLayout<T extends Hittable> extends GridLayout<T> {
    protected int page = 0;
    protected final int items_per_page;
    protected final Option<T>[] page_elements;

    @SuppressWarnings("unchecked")
    public PaginatedGridLayout(AdvancedHitbox hitbox, float hgap, float vgap, int columns, int rows) {
        super(hitbox, hgap, vgap, columns, rows);
        items_per_page = columns * rows;
        page_elements = new Option[items_per_page];
        update_positions();
        change_page(0);
    }

    public boolean can_dec_page() {
        return page > 0;
    }

    public boolean can_inc_page() {
        return page < Math.ceil((float)elements.size()/(float)items_per_page) - 1;
    }

    public void dec_page() {
        change_page(page - 1);
    }

    public void inc_page() {
        change_page(page + 1);
    }

    public void change_page(int page) {
        if (page < 0) return;
        if (page >= Math.ceil((float)elements.size()/(float)items_per_page)) return;
        this.page = page;

        int start = items_per_page * page;
        for (int idx = 0, total_idx = start;
                idx < items_per_page;
                idx++, total_idx++) {
            page_elements[idx] = total_idx < elements.size() ? Some(elements.get(total_idx)) : None();
        }
    }

    @Override
    protected void on_elements_modified() {
        update_positions();
        change_page(page);
    }

    @Override
    protected void update_positions() {
        int column = 0, row = 0;
        for (T element : elements) {
            element.hb.move(cache_x[column++], cache_y[row % rows]);

            column %= columns;
            if (column == 0) row++;
        }
    }

    public int elements_per_page() {
        return items_per_page;
    }

    public Option<T> get_on_page(int idx) {
        return page_elements[idx];
    }

    public void for_each_on_page(Consumer<T> action) {
        Arrays.stream(page_elements).forEach(e -> e.inspect(action));
    }

    @Override
    public void update() {
        Arrays.stream(page_elements).forEach(e -> e.inspect(Renderable::update));
    }

    @Override
    public void render(SpriteBatch sb) {
        hb.render(sb);
        Arrays.stream(page_elements).forEach(e -> e.inspect(f -> f.render(sb)));
    }
}
