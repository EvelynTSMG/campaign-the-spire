package EvesActMod.ui.layouts;

import EvesActMod.ui.Hittable;
import EvesActMod.ui.input.ScrollBar;
import EvesActMod.util.AdvancedHitbox;
import EvesActMod.util.ImageHelper;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class ScrollableGridLayout<T extends Hittable> extends GridLayout<T> {
    protected ScrollBar.ScrollBarDirection direction;
    protected int size;
    protected float[] cache_pos;

    public ScrollableGridLayout(AdvancedHitbox hitbox, ScrollBar.ScrollBarDirection direction, float hgap, float vgap, int size) {
        super(hitbox, hgap, vgap, 1, 1);
        this.direction = direction;
        this.size = size;

        update_position_cache();
    }

    @Override
    public int columns() {
        throw new UnsupportedOperationException("called `GridLayout::columns()` instead of `ScrollableGridLayout::size()` ");
    }

    @Override
    public int rows() {
        throw new UnsupportedOperationException("called `GridLayout::rows()` instead of `ScrollableGridLayout::size()` ");
    }

    @Override
    public void columns(int value) {
        throw new UnsupportedOperationException("called `GridLayout::columns(int)` instead of `ScrollableGridLayout::size(int)` ");
    }

    @Override
    public void rows(int value) {
        throw new UnsupportedOperationException("called `GridLayout::rows(int)` instead of `ScrollableGridLayout::size(int)` ");
    }

    public int size() {
        return size;
    }

    public void size(int value) {
        size = value;
        update_position_cache();
        update_positions();
    }

    @Override
    protected void update_position_cache() {
        if (direction == null) return;
        cache_pos = new float[size];

        switch (direction) {
            case HORIZONTAL:
                float bottom = hb.cY - (size - 1)/2f * vgap;
                cache_pos[size - 1] = bottom;
                for (int i = size - 1; i > 0; cache_pos[i - 1] = cache_pos[i--] + vgap);
                break;
            case VERTICAL:
                float right = hb.cX + (size - 1)/2f * hgap;
                cache_pos[size - 1] = right;
                for (int i = size - 1; i > 0; cache_pos[i - 1] = cache_pos[i--] - hgap);
                break;
        }
    }

    @Override
    protected void update_positions() {
        int idx = 0;
        float pos;

        switch (direction) {
            case HORIZONTAL:
                pos = hb.left() + hgap/2f;
                for (T element : elements) {
                    element.hb.move(pos, cache_pos[idx++]);

                    idx %= size;
                    if (idx == 0) pos += hgap;
                }
                break;
            case VERTICAL:
                pos = hb.top() - vgap/2f;
                for (T element : elements) {
                    element.hb.move(cache_pos[idx++], pos);

                    idx %= size;
                    if (idx == 0) pos -= vgap;
                }
                break;
        }
    }

    @Override
    public void update() {
        hb.update();

        elements.forEach(e -> {
            if (e.hb.intersects(hb))
                e.update();
        });
    }

    @Override
    public void render(SpriteBatch sb) {
        hb.render(sb);

        ImageHelper.scissors(sb, hb, () -> {
            elements.forEach(e -> {
                if (e.hb.intersects(hb))
                    e.render(sb);
            });
        });
    }
}
