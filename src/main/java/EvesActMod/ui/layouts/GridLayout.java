package EvesActMod.ui.layouts;

import EvesActMod.ui.Hittable;
import EvesActMod.ui.Renderable;
import EvesActMod.util.AdvancedHitbox;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.Collection;
import java.util.function.Consumer;

public class GridLayout<T extends Hittable> extends CollectionLayout<T> {
    protected float hgap;
    protected float vgap;
    protected int columns;
    protected int rows;

    protected float[] cache_x;
    protected float[] cache_y;

    public GridLayout(AdvancedHitbox hitbox, float hgap, float vgap, int columns, int rows) {
        if (columns == 0) throw new IllegalArgumentException("`GridLayout::columns` must be greater than 0");
        if (rows == 0) throw new IllegalArgumentException("`GridLayout::rows` must be greater than 0");

        this.hb = hitbox;
        this.hgap = hgap;
        this.vgap = vgap;
        this.columns = columns;
        this.rows = rows;

        update_position_cache();
    }

    public int columns() {
        return columns;
    }

    public int rows() {
        return rows;
    }

    public void columns(int value) {
        columns = value;
        update_position_cache();
        update_positions();
    }

    public void rows(int value) {
        rows = value;
        update_position_cache();
        update_positions();
    }

    protected void update_position_cache() {
        float right = hb.cX + (columns - 1)/2f * hgap;
        float bottom = hb.cY - (rows - 1)/2f * vgap;

        cache_x = new float[columns];
        cache_x[columns - 1] = right;
        for (int i = columns - 1; i > 0; i--) {
            cache_x[i - 1] = cache_x[i] - hgap;
        }

        cache_y = new float[rows];
        cache_y[rows - 1] = bottom;
        for (int i = rows - 1; i > 0; i--) {
            cache_y[i - 1] = cache_y[i] + vgap;
        }
    }

    @Override
    protected void on_elements_modified() {
        update_positions();
    }

    protected void update_positions() {
        int column = 0, row = 0;
        for (T element : elements) {
            element.hb.move(cache_x[column++], cache_y[row]);

            column %= columns;
            if (column == 0) rows++;
        }
    }

    @Override
    public void update() {
        elements.forEach(Renderable::update);
    }

    @Override
    public void render(SpriteBatch sb) {
        hb.render(sb);
        elements.forEach(e -> e.render(sb));
    }
}
