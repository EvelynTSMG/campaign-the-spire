package EvesActMod.ui.buttons;

import EvesActMod.common.Option;
import EvesActMod.util.ImageHelper;
import EvesActMod.util.AdvancedHitbox;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

public abstract class IconButton extends Button {
    public static final Texture ARROW_LEFT = ImageHelper.texture_at("ui/buttons/left_arrow.png");
    public static final Texture ARROW_LEFT_HOVER = ImageHelper.texture_at("ui/buttons/left_arrow_hover.png");
    public static final Texture ARROW_RIGHT = ImageHelper.texture_at("ui/buttons/right_arrow.png");
    public static final Texture ARROW_RIGHT_HOVER = ImageHelper.texture_at("ui/buttons/right_arrow_hover.png");

    public TextureRegion icon;
    public Option<TextureRegion> hover_icon;
    public Option<TextureRegion> click_icon;

    public IconButton(AdvancedHitbox hb, Texture icon, Option<Texture> hover_icon, Option<Texture> click_icon) {
        this.hb = hb;
        this.icon = new TextureRegion(icon);
        this.hover_icon = hover_icon.map(TextureRegion::new);
        this.click_icon = click_icon.map(TextureRegion::new);
    }

    @Override
    public void render(SpriteBatch sb) {
        super.render(sb);

        TextureRegion icon = this.icon;

        if (hb.hovered) {
            icon = hover_icon.unwrap_or(icon);
            if (InputHelper.isMouseDown) icon = click_icon.unwrap_or(icon);
        }

        ImageHelper.draw_centered(sb, icon, hb.cX(), hb.cY(), hb.scale);
    }
}
