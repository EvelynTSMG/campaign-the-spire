package EvesActMod.ui.buttons;

import EvesActMod.ui.Hittable;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.megacrit.cardcrawl.helpers.Hitbox;
import com.megacrit.cardcrawl.helpers.input.InputHelper;

public abstract class Button extends Hittable {
    public void update() {
        hb.update();
        if (hb.hovered && InputHelper.justReleasedClickLeft) on_click();
    }

    public void render(SpriteBatch sb) {
        hb.render(sb);
    }

    public abstract void on_click();
}
