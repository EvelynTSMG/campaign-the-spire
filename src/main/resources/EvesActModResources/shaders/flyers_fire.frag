// Shader License: CC BY 3.0
// Author: Jan Mróz (jaszunio15)
// https://www.shadertoy.com/view/wl2Gzc
// General changes: Formatting, enforce v4.50, adjusted for usecase

#version 450

#define PI 3.1415927
#define TWO_PI 6.2831853
#define EPSILON 0.000001
#define ONE_THIRD 1.0/3.0

#define ANIMATION_SPEED 1.5
#define MOVEMENT_SPEED 1.0
#define MOVEMENT_DIRECTION vec2(0.7, -1.0)

#define PARTICLE_SIZE 0.015

#define PARTICLE_SCALE vec2(0.5, 1.6)
#define PARTICLE_SCALE_VAR vec2(0.25, 0.2)

#define PARTICLE_BLOOM_SCALE vec2(0.5, 0.8)
#define PARTICLE_BLOOM_SCALE_VAR vec2(0.3, 0.1)

#define SPARK_LIGHT 0.54
#define BLOOM_LIGHT 0.42
#define SMOKE_LIGHT 0.44

#define SIZE_MOD 1.05
#define ALPHA_MOD 0.9
#define LAYERS_COUNT 5

uniform sampler2D u_texture;
uniform float u_scale;
uniform vec2 u_screenSize; // Settings.WIDTH, Settings.HEIGHT
uniform vec2 u_imagePos;
uniform vec2 u_imageSize;
uniform vec3 u_charColor;
uniform float u_time;
uniform float u_alpha = 1.0;

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

vec3 rgb2hsl(vec3 c) {
    float h = 0.0;
    float s = 0.0;
    float l = 0.0;
    float r = c.r;
    float g = c.g;
    float b = c.b;
    float cMin = min(r, min(g, b));
    float cMax = max(r, max(g, b));

    l = (cMax + cMin) / 2.0;
    if (cMax > cMin) {
        float cDelta = cMax - cMin;

        s = l < .0 ? cDelta / (cMax + cMin) : cDelta / (2.0 - (cMax + cMin));

        if ( r == cMax ) h = (g - b) / cDelta;
        else if ( g == cMax ) h = 2.0 + (b - r) / cDelta;
        else h = 4.0 + (r - g) / cDelta;

        if (h < 0.0) h += 6.0;
        h = h / 6.0;
    }
    return vec3(h, s, l);
}

vec3 hsl2rgb(vec3 c) {
    vec3 rgb = clamp(abs(mod(c.x * 6.0 + vec3(0.0, 4.0, 2.0), 6.0) - 3.0) - 1.0, 0.0, 1.0);

    return c.z + c.y * (rgb - 0.5) * (1.0 - abs(2.0 * c.z - 1.0));
}

vec3 hsl_with_light(vec3 c, float l) {
    vec3 o = rgb2hsl(c);
    o.z = l;
    return hsl2rgb(o);
}

float hash1_2(vec2 x) {
    return fract(sin(dot(x, vec2(52.127, 61.2871))) * 521.582);
}

vec2 hash2_2(vec2 x) {
    return fract(sin(x * mat2x2(20.52, 24.1994, 70.291, 80.171)) * 492.194);
}

// Simple interpolated noise
vec2 noise2_2(vec2 uv) {
    //vec2 f = fract(uv);
    vec2 f = smoothstep(0.0, 1.0, fract(uv));

    vec2 uv00 = floor(uv);
    vec2 uv01 = uv00 + vec2(0,1);
    vec2 uv10 = uv00 + vec2(1,0);
    vec2 uv11 = uv00 + 1.0;
    vec2 v00 = hash2_2(uv00);
    vec2 v01 = hash2_2(uv01);
    vec2 v10 = hash2_2(uv10);
    vec2 v11 = hash2_2(uv11);

    vec2 v0 = mix(v00, v01, f.y);
    vec2 v1 = mix(v10, v11, f.y);
    vec2 v = mix(v0, v1, f.x);

    return v;
}

// Simple interpolated noise
float noise1_2(vec2 uv) {
    vec2 f = fract(uv);
    //vec2 f = smoothstep(0.0, 1.0, fract(uv));

    vec2 uv00 = floor(uv);
    vec2 uv01 = uv00 + vec2(0,1);
    vec2 uv10 = uv00 + vec2(1,0);
    vec2 uv11 = uv00 + 1.0;

    float v00 = hash1_2(uv00);
    float v01 = hash1_2(uv01);
    float v10 = hash1_2(uv10);
    float v11 = hash1_2(uv11);

    float v0 = mix(v00, v01, f.y);
    float v1 = mix(v10, v11, f.y);
    float v = mix(v0, v1, f.x);

    return v;
}

float layered_noise1_2(vec2 uv, float sizeMod, float alphaMod, int layers, float animation) {
    float noise = 0.0;
    float alpha = 1.0;
    float size = 1.0;
    vec2 offset;

    for (int i = 0; i < layers; i++) {
        offset += hash2_2(vec2(alpha, size)) * 10.0;

        // Adding noise with movement
        noise += noise1_2(uv * size + u_time * animation * 8.0 * MOVEMENT_DIRECTION * MOVEMENT_SPEED + offset) * alpha;
        alpha *= alphaMod;
        size *= sizeMod;
    }

    noise *= (1.0 - alphaMod)/(1.0 - pow(alphaMod, float(layers)));
    return noise;
}

// Rotates point around 0,0
vec2 rotate(vec2 point, float deg) {
    float s = sin(deg);
    float c = cos(deg);
    return mat2x2(s, c, -c, s) * point;
}

// Cell center from point on the grid
vec2 voronoi_point_from_root(vec2 root, float deg) {
    vec2 point = hash2_2(root) - 0.5;
    float s = sin(deg);
    float c = cos(deg);
    point = mat2x2(s, c, -c, s) * point * 0.66;
    point += root + 0.5;
    return point;
}

// Voronoi cell point rotation degrees
float deg_from_root_uv(vec2 uv) {
    return u_time * ANIMATION_SPEED * (hash1_2(uv) - 0.5) * 2.0;
}

vec2 random_around2_2(vec2 point, vec2 range, vec2 uv) {
    return point + (hash2_2(uv) - 0.5) * range;
}

vec3 fire_particles(vec2 uv, vec2 originalUV) {
    vec3 particles = vec3(0.0);
    vec2 rootUV = floor(uv);
    float deg = deg_from_root_uv(rootUV);
    vec2 pointUV = voronoi_point_from_root(rootUV, deg);
    float dist = 2.0;
    float distBloom = 0.0;

    // UV manipulation for the faster particle movement
    vec2 tempUV = uv + (noise2_2(uv * 2.0) - 0.5) * 0.1;
    tempUV += -(noise2_2(uv * 3.0 + u_time) - 0.5) * 0.07;

    // Sparks sdf
    dist = length(rotate(tempUV - pointUV, 0.7) * random_around2_2(PARTICLE_SCALE, PARTICLE_SCALE_VAR, rootUV));

    // Bloom sdf
    distBloom = length(rotate(tempUV - pointUV, 0.7) * random_around2_2(PARTICLE_BLOOM_SCALE, PARTICLE_BLOOM_SCALE_VAR, rootUV));

    // Add sparks
    vec3 sparkColor = hsl_with_light(u_charColor.rgb, SPARK_LIGHT);
    particles += (1.0 - smoothstep(PARTICLE_SIZE * 0.6, PARTICLE_SIZE * 3.0, dist)) * sparkColor;

    // Add bloom
    vec3 bloomColor = hsl_with_light(u_charColor.rgb, BLOOM_LIGHT);
    particles += pow((1.0 - smoothstep(0.0, PARTICLE_SIZE * 6.0, distBloom)) * 1.0, 3.0) * bloomColor;

    // Upper disappear curve randomization
    float border = (hash1_2(rootUV) - 0.5) * 2.0;
    float disappear = 1.0 - smoothstep(border, border + 0.5, originalUV.y);

    // Lower appear curve randomization
    border = (hash1_2(rootUV + 0.214) - 1.8) * 0.7;
    float appear = smoothstep(border, border + 0.4, originalUV.y);

    return particles * disappear * appear;
}


// Layering particles to imitate 3D view
vec3 layered_particles(vec2 uv, float sizeMod, float alphaMod, int layers, float smoke) {
    vec3 particles = vec3(0);
    float size = 1.0;
    float alpha = 1.0;
    vec2 offset = vec2(0.0);
    vec2 noiseOffset;
    vec2 bokehUV;

    for (int i = 0; i < layers; i++) {
        // Particle noise movement
        noiseOffset = (noise2_2(uv * size * 2.0 + 0.5) - 0.5) * 0.15;

        // UV with applied movement
        bokehUV = (uv * size + u_time * MOVEMENT_DIRECTION * MOVEMENT_SPEED) + offset + noiseOffset;

        // Adding particles
        // (if there is more smoke, remove smaller particles)
        particles += fire_particles(bokehUV, uv) * alpha * (1.0 - smoothstep(0.0, 1.0, smoke) * (float(i) / float(layers)));

        // Moving uv origin to avoid generating the same particles
        offset += hash2_2(vec2(alpha, alpha)) * 10.0;

        alpha *= alphaMod;
        size *= sizeMod;
    }

    return particles;
}

void main() {
    vec4 outputColor = v_color * texture2D(u_texture, v_texCoords);
    vec2 uv = (gl_FragCoord.xy - u_imagePos) / u_imageSize;

    float vignette = 1.0 - smoothstep(0.4, 1.4, length(uv + vec2(0.0, 0.3)));

    uv *= 1.8;

    float smokeIntensity = layered_noise1_2(uv * 10.0 + u_time * 4.0 * MOVEMENT_DIRECTION * MOVEMENT_SPEED, 1.7, 0.7, 6, 0.2);
    smokeIntensity *= pow(1.0 - smoothstep(-1.0, 1.6, uv.y), 2.0);
    smokeIntensity *= u_alpha;

    vec3 smokeColor = hsl_with_light(u_charColor.rgb, SMOKE_LIGHT);
    vec3 smoke = smokeIntensity * smokeColor * 0.8 * vignette;

    // Cutting holes in smoke
    smoke *= pow(layered_noise1_2(uv * 4.0 + u_time * 0.5 * MOVEMENT_DIRECTION * MOVEMENT_SPEED, 1.8, 0.5, 3, 0.2), 2.0) * 1.5;

    vec3 particles = layered_particles(uv, SIZE_MOD, ALPHA_MOD, LAYERS_COUNT, smokeIntensity);

    smoke *= u_alpha;
    particles *= u_alpha;

    vec3 col = particles + smoke;

    vec4 fcol = vec4(col, min(1.0, col.r + col.g + col.b));

    outputColor += fcol;
    out_fragColor = outputColor;

}