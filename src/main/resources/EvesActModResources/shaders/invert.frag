#version 450

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

void main() {
    vec4 outputColor = v_color * texture2D(u_texture, v_texCoords);
    out_fragColor = vec4(vec3(1.0) - outputColor.rgb, outputColor.a);
}