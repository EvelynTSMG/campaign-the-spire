#version 450

#define EPSILON 0.000001

uniform sampler2D u_texture;
uniform float u_scale; // Settings.scale
uniform vec2 u_screenSize; // Settings.WIDTH, Settings.HEIGHT
uniform vec2 u_imagePos;
uniform vec2 u_imageSize;
uniform float u_alpha = 1.0;
uniform vec3 u_charColor = vec3(1.0);

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

void main() {
    vec4 outputColor = v_color * texture2D(u_texture, v_texCoords);

    vec2 uv = (gl_FragCoord.xy - u_imagePos + u_imageSize/2.0) / u_imageSize;
    if (uv.x < 0 || uv.y < 0 || uv.x > 1 || uv.y > 1) return;

    vec2 what = uv - 0.5;
    float rf = 1.0 + dot(what, what) * 1.2 * 1.2;

    vec3 col = u_charColor * (1.0 - (1.0 / (rf * rf)));
    col *= u_alpha;
    outputColor += vec4(col.rgb, min(1.0, col.r + col.g + col.b));
    out_fragColor = outputColor;
}