#version 450

#define PI 3.1415927
#define EPSILON 0.000001

// Polar coordinates (angle, speed)
#define MOVEMENT vec2(-1.5, 0.8)
#define MOVEMENT_VARY_SPEED 0.2
#define MOVEMENT_VARY_SCALE 0.3

uniform sampler2D u_texture;
uniform float u_scale; // Settings.scale
uniform vec2 u_screenSize; // Settings.WIDTH, Settings.HEIGHT
uniform vec2 u_imagePos;
uniform vec2 u_imageSize;
uniform float u_time;
uniform float u_alpha = 1.0;
uniform vec3 u_charColor = vec3(0.9);

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

float hash1_2(vec2 x) {
    return fract(sin(dot(x, vec2(52.127, 61.2871))) * 521.582);
}

vec2 hash2_2(vec2 x) {
    return fract(sin(x * mat2x2(20.52, 24.1994, 70.291, 80.171)) * 492.194);
}

// Simple interpolated noise
float noise1_2(vec2 uv) {
    vec2 f = fract(uv);
    //vec2 f = smoothstep(0.0, 1.0, fract(uv));

    vec2 uv00 = floor(uv);
    vec2 uv01 = uv00 + vec2(0,1);
    vec2 uv10 = uv00 + vec2(1,0);
    vec2 uv11 = uv00 + 1.0;

    float v00 = hash1_2(uv00);
    float v01 = hash1_2(uv01);
    float v10 = hash1_2(uv10);
    float v11 = hash1_2(uv11);

    float v0 = mix(v00, v01, f.y);
    float v1 = mix(v10, v11, f.y);
    float v = mix(v0, v1, f.x);

    return v;
}

float layered_noise1_2(vec2 uv, float sizeMod, float alphaMod, float alphaModFix, int layers, float animation) {
    float noise = 0.0;
    float alpha = 1.0;
    float size = 1.0;
    vec2 offset;

    float movement_speed = MOVEMENT.y;

    for (int i = 0; i < layers; i++) {
        offset += hash2_2(vec2(alpha, size)) * 10.0;

        float shifted_x = MOVEMENT.x + sin(u_time * MOVEMENT_VARY_SPEED) * MOVEMENT_VARY_SCALE;
        vec2 movement_dir = vec2(cos(shifted_x), sin(MOVEMENT.x));
        // Adding noise with movement
        noise += (i % 2 == 0 ? 1 : -1) * noise1_2(uv * size + u_time * animation * movement_dir * movement_speed + offset) * alpha;
        if (i % 2 == 0) {
            alpha *= alphaMod;
            size *= sizeMod;
        }
    }

    noise *= (1.0 - alphaModFix)/(1.0 - pow(alphaMod, float(layers)));
    return noise;
}

void main() {
    vec4 outputColor = v_color * texture2D(u_texture, v_texCoords);

    if (outputColor.a < EPSILON) return;

    vec2 uv = (gl_FragCoord.xy - u_imagePos) / u_imageSize;
    float vignette = 1.0 - smoothstep(0.4, 1.4, length(uv + vec2(0.0, 0.3)));

    float smokeIntensity = layered_noise1_2(uv * 10.0, 1.7, 0.5, 0.0, 12, 6.4);
    smokeIntensity *= pow(1.0 - smoothstep(-1.0, 1.6, uv.y), 2.0);
    smokeIntensity *= u_alpha;
    vec3 smoke = smokeIntensity * u_charColor * 0.8 * vignette;
    vec3 col = smoke;
    vec4 fcol = vec4(col, min(1.0, col.r + col.g + col.b));

    outputColor += fcol;
    out_fragColor = outputColor;
}