#version 450

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

void main() {
    vec4 texColor = texture2D(u_texture, v_texCoords);

    float gray = (texColor.r + texColor.g + texColor.b) / 3.0;
    texColor.rgb = vec3(gray);

    out_fragColor = texColor * v_color;
}