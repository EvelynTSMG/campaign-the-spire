#version 450

uniform sampler2D u_texture;

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

void main() {
    out_fragColor = v_color * texture2D(u_texture, v_texCoords);
}