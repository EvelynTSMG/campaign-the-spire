// Adapted from https://godotshaders.com/shader/god-rays/

#version 450

#define PI 3.1415927
#define ANGLE 0.15
#define POSITION -0.4
#define SPREAD 1.2
#define CUTOFF -0.231
#define FALLOFF 1.0
#define FALLOFF_RATE 0.75
#define EDGE_FADE 0.33
#define SPEED 3.0
#define RAY1_DENSITY 10.0
#define RAY2_DENSITY 36.0
#define RAY2_INTENSITY 0.3
#define SEED PI
#define BASE_TIME 6.4
#define ALPHA_MULT 0.6

uniform sampler2D u_texture;
uniform float u_scale;
uniform vec2 u_screenSize; // Settings.WIDTH, Settings.HEIGHT
uniform vec2 u_imagePos;
uniform vec2 u_imageSize;
uniform vec3 u_charColor;
uniform float u_time;
uniform float u_alpha = 1.0;

in vec4 v_color;
in vec2 v_texCoords;

out vec4 out_fragColor;

// Random and noise functions from Book of Shader's chapter on Noise.
float random(vec2 _uv) {
    return fract(sin(dot(_uv.xy,
    vec2(12.9898, 78.233))) *
    43758.5453123);
}

float noise (in vec2 uv) {
    vec2 i = floor(uv);
    vec2 f = fract(uv);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));


    // Smooth Interpolation

    // Cubic Hermine Curve. Same as SmoothStep()
    vec2 u = f * f * (3.0-2.0 * f);

    // Mix 4 coorners percentages
    return mix(a, b, u.x) +
    (c - a)* u.y * (1.0 - u.x) +
    (d - b) * u.x * u.y;
}

mat2 rotate(float _angle){
    return mat2(vec2(cos(_angle), -sin(_angle)),
    vec2(sin(_angle), cos(_angle)));
}

vec4 screen(vec4 base, vec4 blend){
    return 1.0 - (1.0 - base) * (1.0 - blend);
}

float min_in_v(vec3 x) {
    return min(min(x.x, x.y), x.z);
}

float max_in_v(vec3 x) {
    return max(max(x.x, x.y), x.z);
}

void main() {
    vec4 outputColor = v_color * texture2D(u_texture, v_texCoords);
    vec2 uv = (gl_FragCoord.xy - u_imagePos) / u_imageSize;
    vec2 trans_uv = (rotate(ANGLE) * (uv - POSITION)) / ((uv.y + SPREAD) - (uv.y * SPREAD));

    // Animate the ray according the the new transformed UVs
    vec2 ray1 = vec2(trans_uv.x * RAY1_DENSITY + sin((BASE_TIME + u_time) * 0.1 * SPEED) * (RAY1_DENSITY * 0.2) + SEED, 1.0);
    vec2 ray2 = vec2(trans_uv.x * RAY2_DENSITY + sin((BASE_TIME + u_time) * 0.2 * SPEED) * (RAY1_DENSITY * 0.2) + SEED, 1.0);

    // Cut off the ray's edges
    float cut = step(CUTOFF, trans_uv.x) * step(CUTOFF, 1.0 - trans_uv.x);
    ray1 *= cut;
    ray2 *= cut;

    float rays = clamp(noise(ray1) + (noise(ray2) * RAY2_INTENSITY), 0.0, 1.0);

    // Fade out edges
    rays *= smoothstep(0.0, FALLOFF, (1.0 - uv.y * FALLOFF_RATE)); // Bottom
    rays *= smoothstep(0.0 + CUTOFF, EDGE_FADE + CUTOFF, trans_uv.x); // Left
    rays *= smoothstep(0.0 + CUTOFF, EDGE_FADE + CUTOFF, 1.0 - trans_uv.x); // Right

    // Color to the rays
    //vec3 shine = vec3(rays) * vec3(1.0);// * u_charColor.rgb;
    vec3 shine = screen(outputColor, vec4(u_charColor, 1.0)).rgb;
    vec4 col = vec4(shine * rays * u_alpha * ALPHA_MULT, 1.0);
    col.a = (min_in_v(col.rgb) + max_in_v(col.rgb)) / 2.0;
    outputColor += col;
    out_fragColor = outputColor;
}